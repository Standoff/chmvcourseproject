/**
 * ����� ������ ��� ��������� �������
 */
class GraphPlot
{
	private:
		UnicodeString leftAxisTitle;
		UnicodeString bottomAxisTitle;
		TChart * graphArea;
		int quantitySeries;
		vector<TColor> seriesColors;
		vector<UnicodeString> seriesTitles;
		vector< vector<GraphPoint> > series;
		double leftAxisMin;
		double leftAxisMax;
		double bottomAxisMin;
		double bottomAxisMax;
		TStringList * title;
		UnicodeString ERROR_TITLE;
		UnicodeString ERROR_MESSAGE;
		vector<TColor> _getDefaultSeriesColors() {
			//����� �����
			vector<TColor> s;
			s.push_back(clRed);
			s.push_back(clGreen);
			s.push_back(clBlue);
			return s;
		}
		void _setVisibleSeries() {
			for (int i = 0; i < COUNT_SERIES; i++) {
				graphArea->Series[i]->Visible = false;
			}
			if(series.size()) {
				for (int i = 0; i < series.size(); i++) {
					if(series[i].size()) {
						graphArea->Series[i]->Visible = true;
					}
				}
			}
		}
	public:
		static const GRAPH_WIDTH = 3;
		static const COUNT_SERIES = 3;
		GraphPlot(TChart * ga) {
			graphArea = ga;
			title = new TStringList();
			ERROR_MESSAGE = "�� ������� ������ ��� ��������� �������";
			ERROR_TITLE = "������";
		}
		GraphPlot & setTitle(TStringList *t) {
			title = t;
			return * this;
        }
		GraphPlot & setLeftAxisTitle(UnicodeString v) {
			leftAxisTitle = v;
			return * this;
		}
		GraphPlot & setBottomAxisTitle(UnicodeString v) {
			bottomAxisTitle = v;
			return * this;
		}
		GraphPlot & setLeftAxisMinMax(double min, double max) {
			leftAxisMin = min;
			leftAxisMax = max;
			return * this;
		}
		GraphPlot & setBottomAxisMinMax(double min, double max) {
			bottomAxisMin = min;
			bottomAxisMax = max;
			return * this;
		}
		GraphPlot & setSeriesColor(vector< TColor > sc) {
			seriesColors.clear();
			for (int i = 0; i < sc.size(); i++) {
				seriesColors.push_back(sc[i]);
			}
			return * this;
		}
		GraphPlot & setSeriesTitles(vector< UnicodeString > t) {
			seriesTitles.clear();
			seriesTitles = t;
			return * this;
		}
		GraphPlot & setSeries(vector< vector<GraphPoint> > s) {
			quantitySeries = s.size();
			series.clear();
			series = s;
			_setVisibleSeries();
			return * this;
		}
		void plot() {
			//�������� � ������� �������� ����� ����� LABEL
			graphArea->Title->Clear();
			graphArea->Title->Text = title;
			//�������� ����
			graphArea->LeftAxis->Title->Caption = leftAxisTitle;
			graphArea->BottomAxis->Title->Caption = bottomAxisTitle;
			if(!seriesColors.size()) {
            	seriesColors = _getDefaultSeriesColors();
            }
			for (int i = 0; i < quantitySeries; i++) {
				graphArea->Series[i]->Clear();
				graphArea->Series[i]->Color = seriesColors[i];
				graphArea->Series[i]->Title = seriesTitles[i];
				graphArea->Series[i]->Pen->Width = GraphPlot::GRAPH_WIDTH;
			}
			//������������� �������� ���� ����������� �������
			graphArea->LeftAxis->SetMinMax(
				leftAxisMin,
				leftAxisMax
			);
			graphArea->BottomAxis->SetMinMax(
				bottomAxisMin,
				bottomAxisMax
			);
			//������ �������
			int j = series.size();
			if(series.size()) {
				bool atLeastOnePoint = false;
				for (int i = 0; i < series.size(); i++) {
					for (int j = 0; j < series[i].size(); j++) {
					     atLeastOnePoint = true;
						 graphArea->Series[i]->AddXY(
							 series[i][j].getX(),
							 series[i][j].getY()
						 );
					}
				}
				if(!atLeastOnePoint) {
                    graphArea->Refresh();
					Application->MessageBox(ERROR_MESSAGE.w_str(), ERROR_TITLE.w_str(), MB_OK|MB_ICONERROR);
                }
			} else {
				graphArea->Refresh();
				Application->MessageBox(ERROR_MESSAGE.w_str(), ERROR_TITLE.w_str(), MB_OK|MB_ICONERROR);
			}
			graphArea->Refresh();
		}
};
