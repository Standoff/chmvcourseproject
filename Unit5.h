//---------------------------------------------------------------------------

#ifndef Unit5H
#define Unit5H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <Grids.hpp>
#include "Chart.hpp"
#include "Series.hpp"
#include "TeEngine.hpp"
#include "TeeProcs.hpp"
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TChartPlot : public TForm
{
__published:	// IDE-managed Components
	TBitBtn *btn;
	TChart *Chart1;
	TLineSeries *Series2;
	TLineSeries *Series3;
	TBitBtn *BitBtn1;
	TBitBtn *BitBtn2;
	TButton *Button1;
	TButton *Button2;
	TLineSeries *Series1;
	void __fastcall btnClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall BitBtn1Click(TObject *Sender);
	void __fastcall BitBtn2Click(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TChartPlot(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TChartPlot *ChartPlot;
//---------------------------------------------------------------------------
#endif
