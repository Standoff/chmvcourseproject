object Option: TOption
  Left = 629
  Top = 450
  Caption = 'Option'
  ClientHeight = 373
  ClientWidth = 645
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object StaticText1: TStaticText
    Left = 403
    Top = 19
    Width = 163
    Height = 27
    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1087#1086#1084#1077#1093
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object StaticText2: TStaticText
    Left = 99
    Top = 19
    Width = 106
    Height = 27
    Caption = #1069#1082#1089#1087#1086#1079#1080#1094#1080#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object RadioGroup1: TRadioGroup
    Left = 43
    Top = 64
    Width = 230
    Height = 225
    Items.Strings = (
      #1053#1077#1086#1075#1088#1072#1085#1080#1095#1077#1085#1085#1072
      '7 '#1089#1077#1082#1091#1085#1076
      '3 '#1089#1077#1082#1091#1085#1076#1099
      #1053#8594'7'#8594'3 '#1089#1077#1082#1091#1085#1076#1099)
    TabOrder = 2
  end
  object RadioGroup2: TRadioGroup
    Left = 368
    Top = 64
    Width = 233
    Height = 225
    Items.Strings = (
      '10 '#1096#1090#1091#1082
      '15 '#1096#1090#1091#1082
      '25 '#1096#1090#1091#1082
      '10'#8594'15'#8594'25 '#1096#1090#1091#1082)
    TabOrder = 3
  end
  object Button1: TButton
    Left = 526
    Top = 326
    Width = 75
    Height = 25
    Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
    TabOrder = 4
    OnClick = Button1Click
  end
  object CheckBox1: TCheckBox
    Left = 43
    Top = 320
    Width = 270
    Height = 17
    Caption = #1042#1099#1076#1077#1083#1103#1090#1100' '#1094#1074#1077#1090#1086#1084' '#1086#1090#1074#1077#1090#1099' '#1074' '#1090#1072#1073#1083#1080#1094#1077' '#1088#1077#1079#1091#1083#1100#1090#1072#1090#1086#1074
    TabOrder = 5
  end
  object CheckBox2: TCheckBox
    Left = 43
    Top = 343
    Width = 270
    Height = 17
    Caption = #1055#1086#1076#1087#1080#1089#1099#1074#1072#1090#1100' '#1082#1074#1072#1076#1088#1072#1085#1090#1099' '#1074' '#1086#1082#1085#1077' '#1101#1082#1089#1087#1077#1088#1080#1084#1077#1085#1090#1072
    TabOrder = 6
  end
  object Button2: TButton
    Left = 384
    Top = 326
    Width = 89
    Height = 25
    Caption = #1069#1082#1089#1087#1077#1088#1080#1084#1077#1085#1090
    TabOrder = 7
    Visible = False
    OnClick = Button2Click
  end
end
