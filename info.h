#include <vector.h>
#include <string>
#include <math.h>
#include <stdlib.h>
#include "sqlite3.h"
#pragma comment(lib,"sqlite3.lib")
class DbManager {
		sqlite3* db;
		bool createCadrTable() {
            UnicodeString query;
			sqlite3_stmt *stmt = NULL;
			query = "CREATE TABLE cadr (\n\
			  id             integer PRIMARY KEY AUTOINCREMENT NOT NULL,\n\
			  experimentid   integer,\n\
			  mananswer      integer,\n\
			  correctanswer  integer,\n\
			  \"time\"         float(50)\n\
			)";
			sqlite3_prepare_v2(db, (char *) query.t_str(), -1, &stmt, NULL);
			bool flag = false;
			if(sqlite3_step(stmt) != SQLITE_ERROR) {
				 flag = true;
			}
			sqlite3_finalize(stmt);
			return flag;
		}
		bool createExperimentTable() {
			UnicodeString query;
			sqlite3_stmt *stmt = NULL;
			query = "CREATE TABLE experiment (\n\
			  id              integer PRIMARY KEY AUTOINCREMENT NOT NULL,\n\
			  expositionid    integer,\n\
			  quantitynoizes  integer\n\
			)";
			sqlite3_prepare_v2(db, (char *) query.t_str(), -1, &stmt, NULL);
			bool flag = false;
			if(sqlite3_step(stmt) != SQLITE_ERROR) {
				 flag = true;
			}
			sqlite3_finalize(stmt);
			return flag;
		}
		bool createExpositionTable() {
            UnicodeString query;
			sqlite3_stmt *stmt = NULL;
			query = "CREATE TABLE exposition (\n\
			  id        integer PRIMARY KEY AUTOINCREMENT NOT NULL,\n\
			  tryid     integer,\n\
			  duration  integer\n\
			)";
			sqlite3_prepare_v2(db, (char *) query.t_str(), -1, &stmt, NULL);
			bool flag = false;
			if(sqlite3_step(stmt) != SQLITE_ERROR) {
				 flag = true;
			}
			sqlite3_finalize(stmt);
			return flag;
		}
		bool createTrynTable() {
            UnicodeString query;
			sqlite3_stmt *stmt = NULL;
			query = "CREATE TABLE try (\n\
			  id  integer PRIMARY KEY AUTOINCREMENT NOT NULL\n\
			)";
			sqlite3_prepare_v2(db, (char *) query.t_str(), -1, &stmt, NULL);
			bool flag = false;
			if(sqlite3_step(stmt) != SQLITE_ERROR) {
				 flag = true;
			}
			sqlite3_finalize(stmt);
			return flag;
		}
	public:
		static const QUANTITY_TABLES = 4;
		DbManager(sqlite3* database) {
			db = database;
		}
		bool checkExistTables() {
			UnicodeString query;
			sqlite3_stmt *stmt = NULL;
			query = "SELECT count(name) FROM sqlite_master WHERE name IN ('cadr','experiment','exposition','try') ";
			sqlite3_prepare_v2(db, (char *) query.t_str(), -1, &stmt, NULL);
			int quantityTables = 0;
			if(sqlite3_step(stmt) != SQLITE_ERROR) {
				quantityTables = sqlite3_column_int(stmt, 0);
			}
			sqlite3_finalize(stmt);
			return quantityTables == QUANTITY_TABLES ? true : false;
		}
		bool createTables() {
			bool flag = false;
			if(createCadrTable() && createExperimentTable() && createExpositionTable() && createTrynTable()) {
               flag = true;
			}
			return flag;
		}
};
class dbObject {
protected: 
	int id;
	UnicodeString getStrDouble(double number) {
		UnicodeString answer;
		string str = (char *) FloatToStr(number).t_str();
		str.replace(str.find (",",0), 1, ".", 1);
		answer = str.c_str();
		return answer;
    }
public:
	static const NEW_ROW_ID = 0;
	dbObject() {
		id = NEW_ROW_ID;
	}
	void setId(int rowId) {
		id = rowId;
	}
	int getId() {
		return id;
	}
	void save() {}
 };
 /**
 * ����� �������� ������ � ������ ������� � ������
 */
class Cadr : public dbObject {
private:
	/**
	 *  ����� ����������� ���������� ��� ������ ������ ��
	 *  ����� �� �������
	 */
	double time;
	/**
	 *  ����� ������, ������� ��� ������ ����������
	 */
	int manAnswer;
	/**
	 *  ���������� ����� ������, ������� ������ ��� ������� ��������
	 */
	int correctAnswer;
    /**
	 *  ������������� �������� � ��
	 */
	 void saveNew(sqlite3* db, int experimentId) {
		UnicodeString query;
		sqlite3_stmt *stmt = NULL;
		query = "INSERT INTO cadr (experimentid, mananswer, correctanswer, time) VALUES(?,?,?,?)";
		sqlite3_prepare_v2(db, (char *) query.t_str(), -1, &stmt, NULL);
		sqlite3_bind_int(stmt, 1, experimentId);
		sqlite3_bind_int(stmt, 2, manAnswer);
		sqlite3_bind_int(stmt, 3, correctAnswer);
		sqlite3_bind_double(stmt, 4, time);
		if(sqlite3_step(stmt) != SQLITE_ERROR) {
			id = sqlite3_last_insert_rowid(db);
		}
		sqlite3_finalize(stmt);
	 }
public:
	Cadr() : dbObject()  {
		manAnswer = correctAnswer = 0;
	}
	Cadr(double t, int v1, int v2) : dbObject(){
		setCadr(t,v1,v2);
	}
	void setCadr(double t, int v1, int v2) {
		setTime(t);
		setManAnswer(v1);
		setCorrectAnswer(v2);
	}
	void setTime(double v) {
		time = v;
	}
	void setManAnswer(int v) {
		manAnswer = v;
	}
	void setCorrectAnswer(int v) {
		correctAnswer = v;
	}
	double getTime() {
		return time;
	}
	int getManAnswer() {
		return manAnswer;
	}
	int getCorrectAnswer() {
		return correctAnswer;
	}
	void save(sqlite3 *db, int experimentId) {		
		if(!id) {
			saveNew(db, experimentId);
		}
	}
};

/**
 * ����� �������� ������������ ������ � ����� ������
 * vector <Cadr> @results - ������ ������ � ����������� ������
 */
class Experiment : public dbObject {
private:
	vector<Cadr> results;
	void saveNew(sqlite3 *db, int expositionId, int quantityNoizes) {
        UnicodeString query;
		sqlite3_stmt *stmt = NULL;
		query = "INSERT INTO experiment (expositionid, quantitynoizes) VALUES(?,?)";
		sqlite3_prepare_v2(db, (char *) query.t_str(), -1, &stmt, NULL);
		sqlite3_bind_int(stmt, 1, expositionId);
		sqlite3_bind_int(stmt, 2, quantityNoizes);
		if(sqlite3_step(stmt) != SQLITE_ERROR) {
			id = sqlite3_last_insert_rowid(db);
		}
		sqlite3_finalize(stmt);
	}
	void saveCadrs(sqlite3 *db, int experimentId) {
		// at least one defined then cadrs are presents in db
		if(!results[0].getId()) {
			UnicodeString query;
			sqlite3_stmt *stmt = NULL;
			query = "INSERT INTO cadr (experimentid, mananswer, correctanswer, time) VALUES ";
			query += "(";
				query += IntToStr(experimentId);
				query += ",";
				query += IntToStr(results[0].getManAnswer());
				query += ",";
				query += IntToStr(results[0].getCorrectAnswer());
				query += ",";
				query += getStrDouble(results[0].getTime());
			query += ")";
			for(int i = 1; i < results.size(); i++) {
				query += "\n,(";
					query += IntToStr(experimentId);
					query += ",";
					query += IntToStr(results[i].getManAnswer());
					query += ",";
					query += IntToStr(results[i].getCorrectAnswer());
					query += ",";
					query += getStrDouble(results[i].getTime());
				query += ")";
			}
			sqlite3_prepare_v2(db, (char *) query.t_str(), -1, &stmt, NULL);
			sqlite3_step(stmt);
			sqlite3_finalize(stmt);
		}
    }
public:
	static const TEN_NOIZES = 0;

	static const FIFTEEN_NOIZES = 1;
	static const TWENTYFIVE_NOIZES = 2;
	static const COUNT_CARDS = 15;


	Experiment() : dbObject() {}
	// ��������� ���� ���������
	void addCadr(Cadr o) {
		if(results.size() < COUNT_CARDS) {
			results.push_back(o);
		}
	}

	void addCadr(double t, int ma, int ca) {
		if(results.size() < COUNT_CARDS) {
         	Cadr item(t,ma,ca);
			results.push_back(item);
		}
	}
	void addCadr(double t, int ma, int ca, int rowId) {
		if(results.size() < COUNT_CARDS) {
        	Cadr item(t,ma,ca);
			item.setId(rowId);
			results.push_back(item);
		}
	}
	Cadr getCadr(int i) {
		return results[i];
	}
	vector<Cadr> & getCadrs() {
		return results;
	}
	bool isEmpty() {
    	return !((bool) results.size());
    }
	void clear() {
		for (int i = 0; i < results.size(); i++) {
			results[i].~Cadr();
		}
		results.clear();
	}
	void save(sqlite3 *db, int expositionId, int quantityNoizes) {
		if(!isEmpty() && !id) {
			saveNew(db, expositionId, quantityNoizes);
			if(id) {
				saveCadrs(db, id);
			}
		}
	}
};

/**
 * ����� �������� ������ � ����������
 * vector <Cadr> @experiments - ������ ������ � ����������� ������������� (����������� - ������������ ������)
 * � ������������ ����������� ������ ���������� �������������
 */
class Exposition : public dbObject {
private:
	vector<Experiment>experiments;
	void saveNew(sqlite3 *db, int tryId, int duration) {
        UnicodeString query;
		sqlite3_stmt *stmt = NULL;
		query = "INSERT INTO exposition (tryid, duration) VALUES(?,?)";
		sqlite3_prepare_v2(db, (char *) query.t_str(), -1, &stmt, NULL);
		sqlite3_bind_int(stmt, 1, tryId);
		sqlite3_bind_int(stmt, 2, duration);
		if(sqlite3_step(stmt) != SQLITE_ERROR) {
			id = sqlite3_last_insert_rowid(db);
		}
		sqlite3_finalize(stmt);
    }
public:
	static const COUNT_EXPERIMENTS = 3;

	static const INFINITY_EXPOSITION = 0;
	static const SEVEN_EXPOSITION = 1;
	static const THREE_EXPOSITION = 2;

	Exposition() : dbObject() {
		for (int i = 0; i < COUNT_EXPERIMENTS; i++) {
			Experiment n;
			experiments.push_back(n);
		}
	}

	Experiment & getExperiment(int i) {
		return experiments[i];
	}

	void deleteExperiment(int i) {
		experiments[i].clear();
    }
	bool containExperiment(int i) {
		return experiments[i].isEmpty();
    }
	int getExperimentsCount() {
		return COUNT_EXPERIMENTS;
	}
	bool isFill() {
        bool a = true;
		for(int i = 0; i < COUNT_EXPERIMENTS; i++) {
			if(experiments[i].isEmpty()) {
				a = false;
				break;
			}
		}
		return a;
    }
	void save(sqlite3 *db, int tryId, int duration) {
		if(!id) {
			bool leastOneExperimentIsFill = false;
			for(int i = 0; i < COUNT_EXPERIMENTS; i++) {
				if(!experiments[i].isEmpty()) {
					leastOneExperimentIsFill = true;
					break;
				}
			}
			if(leastOneExperimentIsFill) {
            	saveNew(db, tryId, duration);
			}
		}
		if(id) {
        	for(int i = 0; i < COUNT_EXPERIMENTS; i++) {
				experiments[i].save(db, id, i);
			}
		}
	 }
};

/**
 * ����� �������� ������������ ������ � �����������                                                                       �
 * <Experiment> @expositions - ������ ���������� �� ������
 * ���������� ������
 */
class Info : public dbObject {
private:
	vector<Exposition>expositions;
	void saveNew(sqlite3* db) {
		UnicodeString query;
		sqlite3_stmt *stmt = NULL;
		query = "INSERT INTO try (id) VALUES(NULL)";
		sqlite3_prepare_v2(db, (char *) query.t_str(), -1, &stmt, NULL);
		if(sqlite3_step(stmt) != SQLITE_ERROR) {
			id = sqlite3_last_insert_rowid(db);
		}
		sqlite3_finalize(stmt);
	}
public:
	static const COUNT_EXPOSITIONS = 3;
	Info() : dbObject() {
		for (int i = 0; i < COUNT_EXPOSITIONS; i++) {
			Exposition n;
			expositions.push_back(n);
		}
	}
	Exposition & getExposition(int i) {
		return expositions[i];
	}
	int getExpositionsCount() {
		return COUNT_EXPOSITIONS;
	}
	void clear() {
		for (int i = 0; i < COUNT_EXPOSITIONS; i++) {
			for(int j = 0; j < Exposition::COUNT_EXPERIMENTS; j++) {
             	expositions[i].getExperiment(j).clear();
			}
		}
	}
	void save(sqlite3 * db) {
		if(!id) {
			bool leastOneExperimentIsFill = false;
			for (int i = 0; i < COUNT_EXPOSITIONS; i++) {
				for(int j = 0; j < Exposition::COUNT_EXPERIMENTS; j++ ) {
					if(!expositions[i].getExperiment(j).isEmpty()) {
						leastOneExperimentIsFill = true;
						break;
					}
				}
				if(leastOneExperimentIsFill) {
					break;
				}
			}
			if(leastOneExperimentIsFill) {
            	saveNew(db);
			}
		}
		if(id) { 
        	for (int i = 0; i < COUNT_EXPOSITIONS; i++) {
				expositions[i].save(db, id, i);
			}
		}
	}
	bool load(sqlite3 * db) {
		bool flag = false;
		if(id) {
        	UnicodeString query;
			sqlite3_stmt *stmt = NULL;
			query = "SELECT t.id as tryid, en.id as expositionid, et.id as experimentid, en.duration as duration, et.quantitynoizes as quantitynoizes, c.id as cadrid, c.mananswer as mananswer, c.correctanswer as correctanswer, c.time as time\
				FROM cadr as c\
				LEFT JOIN experiment as et ON c.experimentid = et.id\
				LEFT JOIN exposition as en ON et.expositionid = en.id\
				LEFT JOIN try as t ON en.tryid = t.id\
				WHERE t.id = ?\
			";
			sqlite3_prepare_v2(db, (char *) query.t_str(), -1, &stmt, NULL);
			sqlite3_bind_int(stmt, 1, id);
			while(sqlite3_step(stmt) == SQLITE_ROW) {
				flag = true;
				int tryId          = sqlite3_column_int(stmt, 0);
				int expositionId   = sqlite3_column_int(stmt, 1);
				int experimentId   = sqlite3_column_int(stmt, 2);
				int duration       = sqlite3_column_int(stmt, 3);
				int quantityNoizes = sqlite3_column_int(stmt, 4);
				int cadrId         = sqlite3_column_int(stmt, 5);
				int manAnswer      = sqlite3_column_int(stmt, 6);
				int correctAnswer  = sqlite3_column_int(stmt, 7);
				double time        = sqlite3_column_double(stmt, 8);
				expositions[duration].setId(expositionId);
				expositions[duration].getExperiment(quantityNoizes).setId(experimentId);
				expositions[duration].getExperiment(quantityNoizes).addCadr(time, manAnswer, correctAnswer, cadrId);
			}
			sqlite3_finalize(stmt);
		}
		return flag;
	}
};

/** ***************************GRAPHICS!!!************************************** */
class GraphPoint {
private:
	double x, y;

public:
	Point() {
	}

	void setX(double v) {
		x = v;
	}

	void setY(double v) {
		y = v;
	}

	double getX() {
		return x;
	}

	double getY() {
		return y;
	}
};
/**
 * ����� ��������
 */
class ProcessInfo {
public:
	static const CHART_AVERAGE_TIME_OF_QUANTITY_NOIZES = 0;
	static const CHART_FREQUENCY_DETECTION_OF_QUANTITY_NOIZES = 1;
	static const CHART_AVERAGE_TIME_OF_DURATION_EXPOSITIONS = 2;
	static const CHART_FREQUENCY_DETECTION_OF_DURATION_EXPOSITIONS = 3;
	static const CHART_QUANTITY_ERRORS_OF_NUMBER_DISPLAY = 4;

	ProcessInfo(Info &i) {
		info = i;
	}

	vector<vector<GraphPoint> >calcPoints(int type) {
		vector<vector<GraphPoint> >lines;
		switch(type) {
			// ����������� �������� ������� ����������� �������
			// �� ���������� ����� �� ������ ��� ��������� ���������� �������
			case CHART_AVERAGE_TIME_OF_QUANTITY_NOIZES :
					lines = _chartAverageTimeOfQuantityNoizes();
				break;
			case CHART_FREQUENCY_DETECTION_OF_QUANTITY_NOIZES :
				lines = _chartFrequencyDetectionOfQuantityNoizes();
				break;
			case CHART_AVERAGE_TIME_OF_DURATION_EXPOSITIONS :
				lines = _chartAverageTimeOfDurationExpositions();
				break;
			case CHART_FREQUENCY_DETECTION_OF_DURATION_EXPOSITIONS:
				lines = _chartFrequencyDetectionOfDurationExpositions();
				break;
			case CHART_QUANTITY_ERRORS_OF_NUMBER_DISPLAY:
				lines = _chartQuantityErrorsOfNumberDisplay();
				break;
		}
		return lines;
	}
	/*
	 * ��������� ����������� �� �������� @exposition: ��������
	 * � @experiment: ������ ������������
	 */
	double calcProbability(int exposition, int experiment) {
		vector<Cadr>cadrs(info.getExposition(exposition).getExperiment
			(experiment).getCadrs());
		int correctAnswers = 0;
		for (int i = 0; i < Experiment::COUNT_CARDS; i++) {
			if(cadrs[i].getManAnswer() == cadrs[i].getCorrectAnswer()) {
             	correctAnswers++;
			}
		}
		return (correctAnswers / (double) Experiment::COUNT_CARDS);
	}

	/*
	 * ��������� �������� �������� ������� �� �������� @exposition: ��������
	 * � @experiment: ������ ������������
	 */
	double calcAverageTime(int exposition, int experiment) {
		vector<Cadr>cadrs(info.getExposition(exposition).getExperiment
			(experiment).getCadrs());
		double timeSum = 0;
		for (int i = 0; i < Experiment::COUNT_CARDS; i++) {
			timeSum = timeSum + cadrs[i].getTime();
		}
		return timeSum / (double) Experiment::COUNT_CARDS;
	}

private:
	Info info;
	/** ************************************* */
	vector< vector<int> > _getQuantityErrorsFromDisplays() {
		vector< vector<int> > quantityErrors(2, *(new vector<int> (4,0)));
		//���� ������
		for (int i = 0; i < info.getExpositionsCount(); i++) {
			for (int j = 0; j < info.getExposition(i).getExperimentsCount() ; j++) {
				vector<Cadr> cadrs;
				cadrs = info.getExposition(i).getExperiment(j).getCadrs();
				for(int k = 0; k < cadrs.size(); k++) {
					int isIncorrectAnswer = cadrs[k].getManAnswer() == cadrs[k].getCorrectAnswer() ? 0 : 1;
					//��������� ������
					if(isIncorrectAnswer) {
						if(cadrs[k].getCorrectAnswer()) {
							 quantityErrors[0][
								cadrs[k].getCorrectAnswer() - 1
							 ]++;
						} else { //������ ���
							 quantityErrors[1][
								cadrs[k].getManAnswer() - 1
							 ]++;
						}
					}
				}
			}
		}
		return quantityErrors;
    }


	/** ********************************************* */
	// ����������� �������� ������� ����������� ������� 
	// �� ���������� ����� �� ������ ��� ��������� ���������� ������� 
	vector<vector<GraphPoint> >_chartAverageTimeOfQuantityNoizes() {
		vector<vector<GraphPoint> > lines;

		for (int i = 0; i < info.getExpositionsCount(); i++) {
			vector<GraphPoint>points;
			if(info.getExposition(i).isFill()) {
				// ������� ������������
				for (int j = 0; j < info.getExposition(i).getExperimentsCount();
					j++) {
					GraphPoint point;
					switch (j) {
						case 0:
							point.setX(10);
							break;
						case 1:
							point.setX(15);
							break;
						case 2:
							point.setX(25);
							break;
					}
					point.setY(calcAverageTime(i, j));
					points.push_back(point);
				}
			}
			lines.push_back(points);
		}
		return lines;
	}

	// ����������� ������� ����������� �������
	// �� ���������� ����� �� ������ 
	vector<vector<GraphPoint> >_chartFrequencyDetectionOfQuantityNoizes() {
		vector<vector<GraphPoint> >lines;
		// ������� ���������� 
		for (int i = 0; i < info.getExpositionsCount(); i++) {
			vector<GraphPoint>points;
			if(info.getExposition(i).isFill()) {
				// ������� ������������
				for (int j = 0; j < info.getExposition(i).getExperimentsCount(); j++) {
					GraphPoint point;
					switch (j) {
						case 0:
							point.setX(10);
							break;
						case 1:
							point.setX(15);
							break;
						case 2:
							point.setX(25);
							break;
					}
					point.setY(
						calcProbability(i, j)
					);
					points.push_back(point);
				}
			}
			lines.push_back(points);
		}
		return lines;
	}

	// ����������� �������� ������� ����������� �������
	// �� ������������ ����������
	vector<vector<GraphPoint> >_chartAverageTimeOfDurationExpositions() {
		bool allExpositionsIsFill = true;
		vector<vector<GraphPoint> >lines;
		for(int i = 0; i < info.getExpositionsCount(); i++) {
			if(!info.getExposition(i).isFill()) {
				allExpositionsIsFill = false;
			}
		}
		if(allExpositionsIsFill) {
			// ������� ����������
			for (int i = 0; i < info.getExpositionsCount(); i++) {
				vector<GraphPoint>points;
				// ������� ������������
				for (int j = 0; j < info.getExposition(i).getExperimentsCount(); j++) {
					GraphPoint point;
					switch(j) {
						case 0:
							point.setX(30);
							break;
						case 1:
							point.setX(7);
							break;
						case 2:
							point.setX(3);
							break;
					}
					point.setY(
						calcAverageTime(j, i)
					);
					points.push_back(point);
				}
				lines.push_back(points);
			}
		}
		return lines;
	}
	// ����������� ������� ����������� ������� �� ������������ ����������
	vector<vector<GraphPoint> >_chartFrequencyDetectionOfDurationExpositions() {
		bool allExpositionsIsFill = true;
		vector<vector<GraphPoint> >lines;
		for(int i = 0; i < info.getExpositionsCount(); i++) {
			if(!info.getExposition(i).isFill()) {
				allExpositionsIsFill = false;
			}
		}
		if(allExpositionsIsFill) {
			// ������� ����������
			for (int i = 0; i < info.getExpositionsCount(); i++) {
				vector<GraphPoint>points;
				// ������� ������������
				for (int j = 0; j < info.getExposition(i).getExperimentsCount(); j++) {
					GraphPoint point;
					switch(j) {
						case 0:
							point.setX(30);
							break;
						case 1:
							point.setX(7);
							break;
						case 2:
							point.setX(3);
							break;
					}
					point.setY(
						calcProbability(j, i)
					);
					points.push_back(point);
				}
				lines.push_back(points);
			}
		}
		return lines;
	}
	// ����������� ������ �� �������������� ������� �� ������
	vector<vector<GraphPoint> >_chartQuantityErrorsOfNumberDisplay() {
		bool allExpositionsIsFill = true;
		vector<vector<GraphPoint> >lines;
		for(int i = 0; i < info.getExpositionsCount(); i++) {
			if(!info.getExposition(i).isFill()) {
				allExpositionsIsFill = false;
			}
		}
		if(allExpositionsIsFill) {
        	vector< vector<int> > errors = _getQuantityErrorsFromDisplays();
			for (int i = 0; i < 2; i++) {
				vector<GraphPoint> points;
				for (int j = 0; j < 4; j++) {
					GraphPoint point;
					point.setX(j+1);
					point.setY(errors[i][j]);
					points.push_back(point);
				}
				lines.push_back(points);
			}
		}
		return lines;
	}
};

class MathUtil {
public:
	static const int MIN_VALUE = -1000000;
	static const int MAX_VALUE = 1000000;
	static vector< vector<double> > getMinMax(vector<vector<GraphPoint> > & a) {
		vector<double> values;
		values.push_back(MAX_VALUE);
		values.push_back(MIN_VALUE);
		vector< vector<double> > out(2, values);
		if(a.size()) {
			for(int i = 0; i < a.size(); i++) {
				for(int j = 0; j < a[i].size(); j++) {
					double x = a[i][j].getX();
					double y = a[i][j].getY();
					if(out[0][0] > x) {
						out[0][0] = x;
					}
					if (out[0][1] < x) {
						out[0][1] = x;
					}
					if(out[1][0] > y) {
						out[1][0] = y;
					}
					if (out[1][1] < y) {
						out[1][1] = y;
					}
				}
			}
			double correctionX =  fabs(out[0][0] / 10.0);
			double correctionY =  fabs(out[1][1] / 10.0);
			out[0][0] -= correctionX;
			out[0][1] += correctionX;
			out[1][0] -= correctionY;
			out[1][1] += correctionY;
		} else {
			out[0][0] = out[0][1] = out[1][0] = out[1][1] = 0;
		}
		return out;
	}
};

