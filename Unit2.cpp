﻿//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "Unit1.h"
#include "Unit2.h"
#include "Unit3.h"
#include "Unit4.h"
#include "Unit5.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TWorking *Working;
TGridRect GridRect;
//---------------------------------------------------------------------------
int m, n=0, Flag = 0;
int Card = 0;
bool Transfer = false;
int NumNoizes = NULL;
int KindOfExposition = NULL;
int KindOfExperiment = NULL;
int SignalKey = 0;
int ExpositionTimer;
double AnswerTime = 0;
int Exposition_Flag = NULL;
int Experiment_Flag = NULL;
//---------------------------------------------------------------------------
const int Number_Of_Textures = 3;
const int Not_Active = -1;
/*                                                                         */
const int Exposition_N_7_3 = 3;
const int Experiment_10_15_25 = 3;
/*                                                                         */
const int NUM_0 = 0;
const int NUMBER_0 = 48;
const int NUM_NUMBER_0 = 96;
const int NUM_1 = 1;
const int NUMBER_1 = 49;
const int NUM_NUMBER_1 = 97;
const int NUM_2 = 2;
const int NUMBER_2 = 50;
const int NUM_NUMBER_2 = 98;
const int NUM_3 = 3;
const int NUMBER_3 = 51;
const int NUM_NUMBER_3 = 99;
const int NUM_4 = 4;
const int NUMBER_4 = 52;
const int NUM_NUMBER_4 = 100;
/*                                                                         */
const int NOIZE_10 = 10;
const int NOIZE_15 = 15;
const int NOIZE_25 = 25;
const int Exposition_3 = 3;
const int Exposition_7 = 7;
/*                                                                         */
const int MyColCount = 16;
const int MyRowCount = 10;
//---------------------------------------------------------------------------
__fastcall TWorking::TWorking(TComponent* Owner)
	: TForm(Owner)
{
}
//=============================================================================
void TWorking::DrawNorS(TStringGrid *StringGrid)     // Отрисовка помехи/сигнала
{
	int i, v;
	int TKind = rand() % Number_Of_Textures;       // Генерация вида текстуры
	do                                             // Проверка занятости ячейки
	{
		i = rand() % MyColCount;
		v = rand() % MyRowCount;
	}
	while (StringGrid->Cells[i][v] != "");       // "Если ячейка пуста"
	if(TKind == 0)  							   // Текстура № 1
	{
		StringGrid->Cells[i][v] = L"▓";
	}
	if(TKind == 1)  // Текстура № 2
	{
		StringGrid->Cells[i][v] = L"░";
	}
	if(TKind == 2)  // Текстура № 3
	{
		StringGrid->Cells[i][v] = L"█";
	}
}
//---------------------------------------------------------------------------
void TWorking::Cleaner()                             // Очистка полей
{
	int i, v;
	for (i = 0; i < MyColCount; i++)
	{
		for (v = 0; v < MyRowCount; v++)
		{
			StringGrid1->Cells[i][v] = "";
			StringGrid2->Cells[i][v] = "";
			StringGrid3->Cells[i][v] = "";
			StringGrid4->Cells[i][v] = "";
		}
	}
}
//---------------------------------------------------------------------------
void TWorking::Detecter(int Key, int SignalKey, int Exposition, int Experiment)
{
	Timer2->Enabled = false;
	Timer1->Enabled = false;
	info.getExposition(Exposition).getExperiment(Experiment).addCadr(AnswerTime, Key, SignalKey);
	GenCadr();
	Card++;
	Flag = 1;
	Hider();
	Sleep(500);
	Shower();
	Label2->Hide();
	Label1->Show();
	if (KindOfExposition == Exposition::SEVEN_EXPOSITION)
		{
			ExpositionTimer = Exposition_7;
			Timer2->Enabled = true;
		}
	if (KindOfExposition == Exposition::THREE_EXPOSITION)
		{
			ExpositionTimer = Exposition_3;
			Timer2->Enabled = true;
		}
	if (Card == Experiment::COUNT_CARDS)
	{
		Cleaner();
		Card = 0;
		if ((KindOfExposition == Exposition_N_7_3) && (KindOfExperiment == Experiment_10_15_25))
		{
			Experiment_Flag++;
			if (Experiment_Flag < Experiment_10_15_25) 
				{
					ShowMessage("Следующий сэт Помех");
					GenCadr();
					Transfer = true;
				}
			else
				{
					Experiment_Flag = 0;
					Exposition_Flag++;
					if (Exposition_Flag < Exposition_N_7_3) 
					{
						ShowMessage("Следующая экспозиция");
						GenCadr();
						Transfer = true;	
					}
					else
					{
						Cleaner();
						Flag = 0;
						ShowMessage("Эксперимент удачно завершен");
						Exposition_Flag = NULL;
						Experiment_Flag = NULL;
						Working->Close();
					}
				}
		}
		else
		{
			if (KindOfExperiment == Experiment_10_15_25)
			{
				Experiment_Flag++;
				if (Experiment_Flag < Experiment_10_15_25) 
				{
					ShowMessage("Следующий сэт Помех");
					GenCadr();
					Transfer = true;
				}
				else
				{
					Cleaner();
					Flag = 0;
					ShowMessage("Эксперимент удачно завершен");
					Exposition_Flag = NULL;
					Experiment_Flag = NULL;
					Working->Close();
				}
			}
			else
			if (KindOfExposition == Exposition_N_7_3) 
			{
				Exposition_Flag++;
				if (Exposition_Flag < Exposition_N_7_3) 
				{
					ShowMessage("Следующая экспозиция");
					GenCadr();
					Transfer = true;	
				}
				else
				{
					Cleaner();
					Flag = 0;
					ShowMessage("Эксперимент удачно завершен");
					Exposition_Flag = NULL;
					Experiment_Flag = NULL;
					Working->Close();
				}
			}
			else
			{
				Cleaner();
				Flag = 0;
				ShowMessage("Эксперимент удачно завершен");
				Exposition_Flag = NULL;
				Experiment_Flag = NULL;
				Working->Close();
			}
		}
	}
//	else
//	{
//		if (KindOfExposition == Exposition::SEVEN_EXPOSITION)
//				{
//					ExpositionTimer = Exposition_7;
//					//Timer2 -> Enabled = true;
//				}
//				if (KindOfExposition == Exposition::THREE_EXPOSITION)
//				{
//				   //	ShowMessage("FUU");
//					ExpositionTimer = Exposition_3;
//					//Timer2 -> Enabled = true;
//				}
//	GenCadr();
//	Card++;
//	Flag = 1;
//	Hider();
//	Sleep(500);
//	Shower();
//	Label2 -> Hide();
//	Label1 -> Show();
//	}
}
//---------------------------------------------------------------------------
int TWorking::GetNumNoizes()
{
	switch(Experiment_Flag)
	{
		case 0:{NumNoizes = NOIZE_10; break;}
		case 1:{NumNoizes = NOIZE_15; break;}
		case 2:{NumNoizes = NOIZE_25; break;}
	}
	return NumNoizes;
}
//---------------------------------------------------------------------------
int TWorking::GetKindExposition()
{
	KindOfExposition = Option->RadioGroup1->ItemIndex;
	return KindOfExposition;
}
//---------------------------------------------------------------------------
int TWorking::GetKindExperiment()
{
	KindOfExperiment = Option->RadioGroup2->ItemIndex;
	return KindOfExperiment;
}
//---------------------------------------------------------------------------
void TWorking::GenCadr()
{
	Cleaner();
	int NoizesPlace[4];
	int i;
	memset(NoizesPlace, 0, sizeof(NoizesPlace));
	while ((NoizesPlace[0] + NoizesPlace[1] + NoizesPlace[2] + NoizesPlace[3]) < GetNumNoizes())
	{
		int Noize = rand()% 4 ;
		if (NoizesPlace[Noize] < GetNumNoizes()/2)
		{
			NoizesPlace[Noize]++;
		}
	}
	for (i = 0; i < 4; i++)
	{
		for(int j = 0; j < NoizesPlace[i]; j++)
		{
			switch(i)
			{
				case 0:
				{
					DrawNorS(StringGrid1);
					break;
				}
				case 1:
				{
					DrawNorS(StringGrid2);
					break;
				}
				case 2:
				{
					DrawNorS(StringGrid3);
					break;
				}
				case 3:
				{
					DrawNorS(StringGrid4);
					break;
				}
			}
		}
	}
}
//---------------------------------------------------------------------------
int TWorking::GenSignal()
{
	int SignalPlace = rand() % 5;
	switch(SignalPlace)
	{
		case 0:
			break;
		case 1:
		{
			DrawNorS(StringGrid1);
			break;
		}
		case 2:
		{
			DrawNorS(StringGrid2);
			break;
		}
		case 3:
		{
			DrawNorS(StringGrid3);
			break;
		}
		case 4:
		{
			DrawNorS(StringGrid4);
			break;
		}
	}
	return SignalPlace;
}
//---------------------------------------------------------------------------
void TWorking::Shower()                                      // Отображаем поля
{
	Working->StringGrid1->Show();
	Working->StringGrid2->Show();
	Working->StringGrid3->Show();
	Working->StringGrid4->Show();
	Label4->Visible = true;
	Label5->Visible = true;
	Label6->Visible = true;
	Label7->Visible = true;
	//Working ->Repaint();
}
//---------------------------------------------------------------------------
void TWorking::Hider()                                       // Скрываем поля
{
	Working->StringGrid1->Hide();
	Working->StringGrid2->Hide();
	Working->StringGrid3->Hide();
	Working->StringGrid4->Hide();
	Label4->Visible = false;
	Label5->Visible = false;
	Label6->Visible = false;
	Label7->Visible = false;
	Working->Repaint();
}
//=============================================================================
void __fastcall TWorking::StringGrid2Exit(TObject *Sender)
{
	GridRect.Top = -1;
	GridRect.Left = -1;
	GridRect.Right = -1;
	GridRect.Bottom = -1;
	StringGrid1->Selection = GridRect;
}
//---------------------------------------------------------------------------
void __fastcall TWorking::FormShow(TObject *Sender)
{
	srand(time(NULL));
	Label2->Hide();
	GridRect.Top = -1;
	GridRect.Left = -1;
	GridRect.Right = -1;
	GridRect.Bottom = -1;
	StringGrid1->Selection = GridRect;
	StringGrid2->Selection = GridRect;
	StringGrid3->Selection = GridRect;
	StringGrid4->Selection = GridRect;
	Timer1->Enabled = false;
	Timer2->Enabled = false;
	Cleaner();
	Exposition_Flag = 0;
	Exposition_Flag = 0;
	GetKindExposition();
	GetKindExperiment();
	Experiment_Flag = KindOfExperiment;
	Exposition_Flag = KindOfExposition;
	Starter();
}
//---------------------------------------------------------------------------
void TWorking::Starter()
{
 	if (( KindOfExperiment != Not_Active ) && ( KindOfExposition != Not_Active ))
	{
		Shower();
	   //	ShowMessage(String(Option->RadioGroup1->Text));
		Label1 -> Show();
		if ((KindOfExposition == Exposition_N_7_3) && (KindOfExperiment == Experiment_10_15_25))
		{
			Exposition_Flag = 0;
			Experiment_Flag = 0;
		}
		else
		{
			if (KindOfExposition == Exposition_N_7_3)
				Exposition_Flag = 0;
			if (KindOfExperiment == Experiment_10_15_25)
				Experiment_Flag = 0;
		}
		GenCadr();
		Flag = 1;
	}
	if (KindOfExposition == Exposition::SEVEN_EXPOSITION)
		{
			ExpositionTimer = Exposition_7;
			Timer2->Enabled = true;
		}
	if (KindOfExposition == Exposition::THREE_EXPOSITION)
		{
			ExpositionTimer = Exposition_3;
			Timer2->Enabled = true;
		}
}
//---------------------------------------------------------------------------
void __fastcall TWorking::StringGrid2MouseWheelDown(TObject *Sender, TShiftState Shift,
		  TPoint &MousePos, bool &Handled)
{
	Handled = True;
}
//---------------------------------------------------------------------------
void __fastcall TWorking::StringGrid2MouseWheelUp(TObject *Sender, TShiftState Shift,
		  TPoint &MousePos, bool &Handled)
{
	Handled = True;
}
//---------------------------------------------------------------------------
void __fastcall TWorking::StringGrid2DrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect,
		  TGridDrawState State)
{
	UINT TextFlags = DT_CENTER | DT_VCENTER | DT_SINGLELINE;
	((TStringGrid*)Sender)->Canvas->FillRect(Rect);
	DrawText(((TStringGrid*)Sender)->Canvas->Handle,
	((TStringGrid*)Sender)->Cells[ACol][ARow].c_str(),
	((TStringGrid*)Sender)->Cells[ACol][ARow].Length(), &Rect,TextFlags);
}
//---------------------------------------------------------------------------
void __fastcall TWorking::StringGrid1Exit(TObject *Sender)
{
	GridRect.Top = -1;
	GridRect.Left = -1;
	GridRect.Right = -1;
	GridRect.Bottom = -1;
	StringGrid2->Selection = GridRect;
}
//---------------------------------------------------------------------------
void __fastcall TWorking::StringGrid3Exit(TObject *Sender)
{
	GridRect.Top = -1;
	GridRect.Left = -1;
	GridRect.Right = -1;
	GridRect.Bottom = -1;
	StringGrid3->Selection = GridRect;
}
//---------------------------------------------------------------------------
void __fastcall TWorking::StringGrid4Exit(TObject *Sender)
{
	GridRect.Top = -1;
	GridRect.Left = -1;
	GridRect.Right = -1;
	GridRect.Bottom = -1;
	StringGrid4->Selection = GridRect;
}
//---------------------------------------------------------------------------
void __fastcall TWorking::StringGrid1MouseWheelDown(TObject *Sender, TShiftState Shift,
          TPoint &MousePos, bool &Handled)
{
	Handled = True;
}
//---------------------------------------------------------------------------
void __fastcall TWorking::StringGrid1MouseWheelUp(TObject *Sender, TShiftState Shift,
          TPoint &MousePos, bool &Handled)
{
	Handled = True;
}
//---------------------------------------------------------------------------
void __fastcall TWorking::StringGrid3MouseWheelDown(TObject *Sender, TShiftState Shift,
          TPoint &MousePos, bool &Handled)
{
	Handled = True;
}
//---------------------------------------------------------------------------
void __fastcall TWorking::StringGrid3MouseWheelUp(TObject *Sender, TShiftState Shift,
          TPoint &MousePos, bool &Handled)
{
	Handled = True;
}
//---------------------------------------------------------------------------
void __fastcall TWorking::StringGrid4MouseWheelDown(TObject *Sender, TShiftState Shift,
          TPoint &MousePos, bool &Handled)
{
	Handled = True;
}
//---------------------------------------------------------------------------
void __fastcall TWorking::StringGrid4MouseWheelUp(TObject *Sender, TShiftState Shift,
          TPoint &MousePos, bool &Handled)
{
	Handled = True;
}
//---------------------------------------------------------------------------
void __fastcall TWorking::StringGrid1DrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect,
          TGridDrawState State)
{
	UINT TextFlags = DT_CENTER | DT_VCENTER | DT_SINGLELINE;
	((TStringGrid*)Sender)->Canvas->FillRect(Rect);
	DrawText(((TStringGrid*)Sender)->Canvas->Handle,
	((TStringGrid*)Sender)->Cells[ACol][ARow].c_str(),
	((TStringGrid*)Sender)->Cells[ACol][ARow].Length(), &Rect,TextFlags);
}
//---------------------------------------------------------------------------
void __fastcall TWorking::StringGrid3DrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect,
          TGridDrawState State)
{
	UINT TextFlags = DT_CENTER | DT_VCENTER | DT_SINGLELINE;
	((TStringGrid*)Sender)->Canvas->FillRect(Rect);
	DrawText(((TStringGrid*)Sender)->Canvas->Handle,
	((TStringGrid*)Sender)->Cells[ACol][ARow].c_str(),
	((TStringGrid*)Sender)->Cells[ACol][ARow].Length(), &Rect,TextFlags);
}
//---------------------------------------------------------------------------
void __fastcall TWorking::StringGrid4DrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect,
          TGridDrawState State)
{
	UINT TextFlags = DT_CENTER | DT_VCENTER | DT_SINGLELINE;
	((TStringGrid*)Sender)->Canvas->FillRect(Rect);
	DrawText(((TStringGrid*)Sender)->Canvas->Handle,
	((TStringGrid*)Sender)->Cells[ACol][ARow].c_str(),
	((TStringGrid*)Sender)->Cells[ACol][ARow].Length(), &Rect,TextFlags);
}
//---------------------------------------------------------------------------
void __fastcall TWorking::StringGrid2SelectCell(TObject *Sender, int ACol, int ARow,
          bool &CanSelect)
{
	CanSelect = false;
}
//---------------------------------------------------------------------------
void __fastcall TWorking::StringGrid1SelectCell(TObject *Sender, int ACol, int ARow,
          bool &CanSelect)
{
	CanSelect = false;
}
//---------------------------------------------------------------------------
void __fastcall TWorking::StringGrid3SelectCell(TObject *Sender, int ACol, int ARow,
		  bool &CanSelect)
{
	CanSelect = false;
}
//---------------------------------------------------------------------------
void __fastcall TWorking::StringGrid4SelectCell(TObject *Sender, int ACol, int ARow,
		  bool &CanSelect)
{
	CanSelect = false;
}
//---------------------------------------------------------------------------
void TWorking::Experiment(int Key, int KindOfExposition, int KindOfExperiment)
{
	if (Key == VK_ESCAPE)
	{
		Card = 0;
		if (Flag != 0)
		{
			ShowMessage("Эксперимент прерван, данные не сохранены");
			info.getExposition(KindOfExposition).deleteExperiment(KindOfExperiment);
			Working->Close();
		}
		Flag = 0;
		Cleaner();
	}
	if (Flag != 0)
	{
		if (Card < Experiment::COUNT_CARDS)
		{
			if ((Key != NULL)&&(Flag == 1))
			{
				if (Transfer == false)
				{
					Hider();
					Flag = 2;
					SignalKey = GenSignal();
					Label3->Hide();
					Label2->Show();
				}
				else
				{	
					Transfer = false;
                }
			}
			else if ((Key != NULL)&&(Flag == 2))
			{
				Shower();
				Flag = 3;
				Key = NULL;
				AnswerTime = 0;
				Timer1->Enabled = true;
			}
		}
	}
	if (Flag == 3)
	{
		if((Key == NUMBER_1)||(Key == NUM_NUMBER_1))
			{
				Detecter(NUM_1, SignalKey, KindOfExposition, KindOfExperiment);
			}
		if((Key == NUMBER_2)||(Key == NUM_NUMBER_2))
			{
				Detecter(NUM_2, SignalKey, KindOfExposition, KindOfExperiment);
			}
		if((Key == NUMBER_3)||(Key == NUM_NUMBER_3))
			{
				Detecter(NUM_3, SignalKey, KindOfExposition, KindOfExperiment);
			}
		if((Key == NUMBER_4)||(Key == NUM_NUMBER_4))
			{
				Detecter(NUM_4, SignalKey, KindOfExposition, KindOfExperiment);
			}
		if((Key == NUMBER_0)||(Key == NUM_NUMBER_0))
			{
				Detecter(NUM_0, SignalKey, KindOfExposition, KindOfExperiment);
			}
	}
}
//---------------------------------------------------------------------------
void __fastcall TWorking::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
	if (Flag != 0)
	{
        if ((KindOfExposition == Exposition_N_7_3) && (KindOfExperiment == Experiment_10_15_25))
			{
				Experiment(Key, Exposition_Flag, Experiment_Flag);
			}
		else
		{
			if (KindOfExposition == Exposition_N_7_3)
				{
					if (Exposition_Flag == Exposition::INFINITY_EXPOSITION) 
					{
						Experiment(Key, Exposition::INFINITY_EXPOSITION, KindOfExperiment);
					}
					if (Exposition_Flag == Exposition::SEVEN_EXPOSITION) 
					{
						Experiment(Key, Exposition::SEVEN_EXPOSITION, KindOfExperiment);
					}
						if (Exposition_Flag == Exposition::THREE_EXPOSITION) 
					{
						Experiment(Key, Exposition::THREE_EXPOSITION, KindOfExperiment);
					}
				}
			else
				if (KindOfExperiment == Experiment_10_15_25)
					{
						if (Experiment_Flag == Experiment::TEN_NOIZES)
						{
						   Experiment(Key, KindOfExposition, Experiment::TEN_NOIZES);
						}
						if (Experiment_Flag == Experiment::FIFTEEN_NOIZES)
						{
							Experiment(Key, KindOfExposition, Experiment::FIFTEEN_NOIZES);
						}
						if (Experiment_Flag == Experiment::TWENTYFIVE_NOIZES)
						{
							Experiment(Key, KindOfExposition, Experiment::TWENTYFIVE_NOIZES);
						}
					}
			else
				{
					Experiment(Key, KindOfExposition, KindOfExperiment);
				}
		 }
	}
}
//---------------------------------------------------------------------------
void __fastcall TWorking::Button3Click(TObject *Sender)
{
	Option->ShowModal();
}
//---------------------------------------------------------------------------
void __fastcall TWorking::Timer1Timer(TObject *Sender)
{
	AnswerTime += 0.01 * (double)Timer1->Interval;
}
//---------------------------------------------------------------------------
void __fastcall TWorking::Timer2Timer(TObject *Sender)
{
	ExpositionTimer--;
	if ((ExpositionTimer == 0) && (Timer2->Enabled == true))
	{
		Timer2->Enabled = false;
		Hider();
		Flag = 3;
	  //	ShowMessage("Ваше время истекло");
	}
}
//---------------------------------------------------------------------------
void __fastcall TWorking::FormClose(TObject *Sender, TCloseAction &Action)
{
	Card = 0;
	if (Flag != 0)
	{
		ShowMessage("Эксперимент прерван некорректно, данные не были сохранены");
	 //	info.getExposition(KindOfExposition).deleteExperiment(KindOfExperiment);
		Cleaner();
	}
	Flag = 0;
	Cleaner();
}
//---------------------------------------------------------------------------


void __fastcall TWorking::FormCreate(TObject *Sender)
{
	info.setId(8);
	info.load(BaseForm->db);
}
//---------------------------------------------------------------------------

