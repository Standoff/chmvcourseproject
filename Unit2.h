//---------------------------------------------------------------------------
#ifndef Unit2H
#define Unit2H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Grids.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TWorking : public TForm
{
__published:	// IDE-managed Components
	TStringGrid *StringGrid2;
	TStringGrid *StringGrid1;
	TStringGrid *StringGrid3;
	TStringGrid *StringGrid4;
	TStatusBar *StatusBar1;
	TTimer *Timer1;
	TLabel *Label1;
	TLabel *Label2;
	TTimer *Timer2;
	TLabel *Label3;
	TLabel *Label4;
	TLabel *Label5;
	TLabel *Label6;
	TLabel *Label7;
	void __fastcall StringGrid2Exit(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall StringGrid2MouseWheelDown(TObject *Sender, TShiftState Shift, TPoint &MousePos,
          bool &Handled);
	void __fastcall StringGrid2MouseWheelUp(TObject *Sender, TShiftState Shift, TPoint &MousePos,
		  bool &Handled);
	void __fastcall StringGrid2DrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect,
		  TGridDrawState State);
	void __fastcall StringGrid1Exit(TObject *Sender);
	void __fastcall StringGrid3Exit(TObject *Sender);
	void __fastcall StringGrid4Exit(TObject *Sender);
	void __fastcall StringGrid1MouseWheelDown(TObject *Sender, TShiftState Shift, TPoint &MousePos,
          bool &Handled);
	void __fastcall StringGrid1MouseWheelUp(TObject *Sender, TShiftState Shift, TPoint &MousePos,
          bool &Handled);
	void __fastcall StringGrid3MouseWheelDown(TObject *Sender, TShiftState Shift, TPoint &MousePos,
		  bool &Handled);
	void __fastcall StringGrid3MouseWheelUp(TObject *Sender, TShiftState Shift, TPoint &MousePos,
          bool &Handled);
	void __fastcall StringGrid4MouseWheelDown(TObject *Sender, TShiftState Shift, TPoint &MousePos,
          bool &Handled);
	void __fastcall StringGrid4MouseWheelUp(TObject *Sender, TShiftState Shift, TPoint &MousePos,
          bool &Handled);
	void __fastcall StringGrid1DrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect,
          TGridDrawState State);
	void __fastcall StringGrid3DrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect,
          TGridDrawState State);
	void __fastcall StringGrid4DrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect,
          TGridDrawState State);
	void __fastcall StringGrid2SelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect);
	void __fastcall StringGrid1SelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect);
	void __fastcall StringGrid3SelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect);
	void __fastcall StringGrid4SelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Timer1Timer(TObject *Sender);
	void __fastcall Timer2Timer(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall FormCreate(TObject *Sender);


private:	// User declarations
public:		// User declarations
	__fastcall TWorking(TComponent* Owner);
	void DrawNorS(TStringGrid *StringGrid);
	void GenCadr();
	int GenSignal();
	void Detecter(int Key, int SignalKey, int Exposition, int Experiment);
	void Shower();
	void Hider();
	void Cleaner();
	int GetNumNoizes();
	int GetKindExposition();
	int GetKindExperiment();
	void Starter();
	void Experiment(int Key, int KindOfExposition, int KindOfExperiment);
	Info info;
};
//---------------------------------------------------------------------------
extern PACKAGE TWorking *Working;
//---------------------------------------------------------------------------
#endif
