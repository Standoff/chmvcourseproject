//---------------------------------------------------------------------------
#ifndef Unit3H
#define Unit3H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TMatrix : public TForm
{
__published:	// IDE-managed Components
	TStringGrid *StringGrid1;
	TPanel *Panel1;
	TStringGrid *StringGrid3;
	TPanel *Panel2;
	TPanel *Panel3;
	TPanel *Panel4;
	TPanel *Panel5;
	TPanel *Panel6;
	TPanel *Panel7;
	TPanel *Panel8;
	TPanel *Panel9;
	TPanel *Panel10;
	TPanel *Panel11;
	TPanel *Panel12;
	TPanel *Panel13;
	TStatusBar *StatusBar1;
	TShape *Shape1;
	TStringGrid *StringGrid2;
	TPanel *Panel14;
	TPanel *Panel15;
	TStaticText *StaticText1;
	TStaticText *StaticText2;
	TStaticText *StaticText3;
	TStaticText *StaticText4;
	TStaticText *StaticText5;
	TStaticText *StaticText6;
	TStaticText *StaticText7;
	TStaticText *StaticText8;
	TStaticText *StaticText9;
	TStaticText *StaticText10;
	TStaticText *StaticText11;
	TStaticText *StaticText12;
	TStaticText *StaticText13;
	TStaticText *StaticText14;
	TStaticText *StaticText15;
	TEdit *probability10;
	TEdit *averageTime10;
	TEdit *probability15;
	TEdit *averageTime15;
	TEdit *probability25;
	TEdit *averageTime25;
	TRadioButton *RadioButton1;
	TRadioButton *RadioButton2;
	TRadioButton *RadioButton3;
	void __fastcall StringGrid1MouseWheelUp(TObject *Sender, TShiftState Shift, TPoint &MousePos,
          bool &Handled);
	void __fastcall StringGrid1MouseWheelDown(TObject *Sender, TShiftState Shift, TPoint &MousePos,
          bool &Handled);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall StringGrid2MouseWheelDown(TObject *Sender, TShiftState Shift, TPoint &MousePos,
          bool &Handled);
	void __fastcall StringGrid2MouseWheelUp(TObject *Sender, TShiftState Shift, TPoint &MousePos,
          bool &Handled);
	void __fastcall StringGrid3MouseWheelDown(TObject *Sender, TShiftState Shift, TPoint &MousePos,
          bool &Handled);
	void __fastcall StringGrid3MouseWheelUp(TObject *Sender, TShiftState Shift, TPoint &MousePos,
          bool &Handled);
	void __fastcall StringGrid4MouseWheelDown(TObject *Sender, TShiftState Shift, TPoint &MousePos,
          bool &Handled);
	void __fastcall StringGrid4MouseWheelUp(TObject *Sender, TShiftState Shift, TPoint &MousePos,
		  bool &Handled);
	void __fastcall StringGrid1Exit(TObject *Sender);
	void __fastcall StringGrid2Exit(TObject *Sender);
	void __fastcall StringGrid3Exit(TObject *Sender);
	void __fastcall StringGrid1DrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect,
		  TGridDrawState State);
	void __fastcall StringGrid2DrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect,
		  TGridDrawState State);
	void __fastcall StringGrid3DrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect,
		  TGridDrawState State);
	void __fastcall StringGrid1SelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect);
	void __fastcall StringGrid2SelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect);
	void __fastcall StringGrid3SelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect);
	void __fastcall RadioButton1Click(TObject *Sender);
	void __fastcall RadioButton2Click(TObject *Sender);
	void __fastcall RadioButton3Click(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);



private:	// User declarations
public:		// User declarations
	__fastcall TMatrix(TComponent* Owner);
	void Drawer(TStringGrid *StringGrid, int Cadr, int Experiment, int Exposition);
	void DrawerBottomValues(TEdit *p, TEdit *at, int exprt, int expstn);
	void Cleaner();
	void OptionChecker ();
};
//---------------------------------------------------------------------------
extern PACKAGE TMatrix *Matrix;
//---------------------------------------------------------------------------
#endif
