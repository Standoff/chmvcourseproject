//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include <ComCtrls.hpp>
#include "info.h"
//---------------------------------------------------------------------------
class TBaseForm : public TForm
{
__published:	// IDE-managed Components
	TMainMenu *MainMenu1;
	TMenuItem *Options;
	TMenuItem *Work;
	TMenuItem *Experiment;
	TMenuItem *Theory;
	TMenuItem *Results;
	TMenuItem *Charts;
	TMenuItem *Graphics;
	TStatusBar *StatusBar1;
	void __fastcall ChartsClick(TObject *Sender);
	void __fastcall ExperimentClick(TObject *Sender);
	void __fastcall OptionsClick(TObject *Sender);
	void __fastcall GraphicsClick(TObject *Sender);
	void __fastcall TheoryClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TBaseForm(TComponent* Owner);
	sqlite3* db;
};
//---------------------------------------------------------------------------
extern PACKAGE TBaseForm *BaseForm;
//---------------------------------------------------------------------------
#endif
