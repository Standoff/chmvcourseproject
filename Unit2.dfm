object Working: TWorking
  Left = 343
  Top = 193
  Caption = 'Working'
  ClientHeight = 595
  ClientWidth = 884
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 288
    Top = 4
    Width = 308
    Height = 16
    Caption = #1044#1083#1103' '#1087#1088#1086#1076#1086#1083#1078#1077#1085#1080#1103' '#1088#1072#1073#1086#1090#1099' '#1085#1072#1078#1084#1080#1090#1077' '#1083#1102#1073#1091#1102' '#1082#1083#1072#1074#1080#1096#1091
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 168
    Top = 22
    Width = 543
    Height = 16
    Caption = 
      #1042#1074#1077#1076#1080#1090#1077' '#1085#1086#1084#1077#1088' '#1082#1074#1072#1076#1088#1072#1085#1090#1072', '#1074' '#1082#1086#1090#1086#1088#1086#1084' '#1087#1086#1103#1074#1080#1090#1089#1103' '#1089#1080#1075#1085#1072#1083'. '#1042#1074#1077#1076#1080#1090#1077' 0, '#1077 +
      #1089#1083#1080' '#1089#1080#1075#1085#1072#1083' '#1085#1077' '#1087#1086#1103#1074#1080#1083#1089#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 312
    Top = 22
    Width = 254
    Height = 16
    Caption = #1047#1072#1087#1086#1084#1085#1080#1090#1077' '#1088#1072#1089#1087#1086#1083#1086#1078#1077#1085#1080#1077' '#1087#1086#1084#1077#1093' '#1085#1072' '#1101#1082#1088#1072#1085#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 25
    Top = 35
    Width = 75
    Height = 13
    Caption = #1050#1074#1072#1076#1088#1072#1085#1090' '#8470' 2'
  end
  object Label5: TLabel
    Left = 784
    Top = 35
    Width = 75
    Height = 13
    Caption = #1050#1074#1072#1076#1088#1072#1085#1090' '#8470' 1'
  end
  object Label6: TLabel
    Left = 25
    Top = 304
    Width = 75
    Height = 13
    Caption = #1050#1074#1072#1076#1088#1072#1085#1090' '#8470' 3'
  end
  object Label7: TLabel
    Left = 784
    Top = 304
    Width = 75
    Height = 13
    Caption = #1050#1074#1072#1076#1088#1072#1085#1090' '#8470' 4'
  end
  object StringGrid2: TStringGrid
    Left = 24
    Top = 48
    Width = 403
    Height = 253
    ColCount = 16
    DefaultColWidth = 24
    FixedCols = 0
    RowCount = 10
    FixedRows = 0
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine]
    ParentFont = False
    TabOrder = 0
    OnDrawCell = StringGrid2DrawCell
    OnExit = StringGrid2Exit
    OnMouseWheelDown = StringGrid2MouseWheelDown
    OnMouseWheelUp = StringGrid2MouseWheelUp
    OnSelectCell = StringGrid2SelectCell
  end
  object StringGrid1: TStringGrid
    Left = 456
    Top = 48
    Width = 403
    Height = 253
    ColCount = 16
    DefaultColWidth = 24
    FixedCols = 0
    RowCount = 10
    FixedRows = 0
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine]
    ParentFont = False
    TabOrder = 1
    OnDrawCell = StringGrid1DrawCell
    OnExit = StringGrid1Exit
    OnMouseWheelDown = StringGrid1MouseWheelDown
    OnMouseWheelUp = StringGrid1MouseWheelUp
    OnSelectCell = StringGrid1SelectCell
  end
  object StringGrid3: TStringGrid
    Left = 24
    Top = 317
    Width = 403
    Height = 253
    ColCount = 16
    DefaultColWidth = 24
    FixedCols = 0
    RowCount = 10
    FixedRows = 0
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine]
    ParentFont = False
    TabOrder = 2
    OnDrawCell = StringGrid3DrawCell
    OnExit = StringGrid3Exit
    OnMouseWheelDown = StringGrid3MouseWheelDown
    OnMouseWheelUp = StringGrid3MouseWheelUp
    OnSelectCell = StringGrid3SelectCell
  end
  object StringGrid4: TStringGrid
    Left = 456
    Top = 317
    Width = 403
    Height = 253
    ColCount = 16
    DefaultColWidth = 24
    FixedCols = 0
    RowCount = 10
    FixedRows = 0
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine]
    ParentFont = False
    TabOrder = 3
    OnDrawCell = StringGrid4DrawCell
    OnExit = StringGrid4Exit
    OnMouseWheelDown = StringGrid4MouseWheelDown
    OnMouseWheelUp = StringGrid4MouseWheelUp
    OnSelectCell = StringGrid4SelectCell
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 576
    Width = 884
    Height = 19
    Panels = <
      item
        Alignment = taCenter
        Bevel = pbNone
        BiDiMode = bdLeftToRight
        ParentBiDiMode = False
        Text = 
          #1042#1085#1080#1084#1072#1085#1080#1077'! '#1048#1076#1077#1090' '#1087#1088#1086#1074#1077#1076#1077#1085#1080#1077' '#1101#1082#1089#1087#1077#1088#1080#1084#1077#1085#1090#1072'.                         ' +
          '                                        '#1044#1083#1103' '#1074#1099#1093#1086#1076#1072', '#1085#1072#1078#1084#1080#1090#1077' ESC'
        Width = 50
      end>
  end
  object Timer1: TTimer
    Interval = 1
    OnTimer = Timer1Timer
    Left = 136
    Top = 8
  end
  object Timer2: TTimer
    OnTimer = Timer2Timer
    Left = 720
    Top = 8
  end
end
