object Matrix: TMatrix
  Left = 343
  Top = 129
  Caption = 'Matrix'
  ClientHeight = 616
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Shape1: TShape
    Left = 24
    Top = 87
    Width = 145
    Height = 392
    Pen.Color = clSilver
    Pen.Style = psInsideFrame
    Pen.Width = 0
  end
  object StringGrid1: TStringGrid
    Left = 175
    Top = 87
    Width = 200
    Height = 392
    Cursor = crArrow
    ColCount = 3
    DoubleBuffered = False
    FixedCols = 0
    RowCount = 15
    FixedRows = 0
    GridLineWidth = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine]
    ParentDoubleBuffered = False
    ScrollBars = ssNone
    TabOrder = 0
    Touch.ParentTabletOptions = False
    Touch.TabletOptions = [toPressAndHold]
    OnDrawCell = StringGrid1DrawCell
    OnExit = StringGrid1Exit
    OnMouseWheelDown = StringGrid1MouseWheelDown
    OnMouseWheelUp = StringGrid1MouseWheelUp
    OnSelectCell = StringGrid1SelectCell
    ColWidths = (
      64
      64
      64)
  end
  object Panel1: TPanel
    Left = 24
    Top = 24
    Width = 145
    Height = 57
    Caption = #1053#1086#1084#1077#1088' '#1082#1072#1076#1088#1072
    TabOrder = 1
  end
  object StringGrid3: TStringGrid
    Left = 587
    Top = 87
    Width = 200
    Height = 392
    Cursor = crArrow
    ColCount = 3
    DoubleBuffered = False
    FixedCols = 0
    RowCount = 15
    FixedRows = 0
    GridLineWidth = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine]
    ParentDoubleBuffered = False
    TabOrder = 2
    OnDrawCell = StringGrid3DrawCell
    OnExit = StringGrid3Exit
    OnMouseWheelDown = StringGrid3MouseWheelDown
    OnMouseWheelUp = StringGrid3MouseWheelUp
    OnSelectCell = StringGrid3SelectCell
    ColWidths = (
      64
      64
      64)
  end
  object Panel2: TPanel
    Left = 175
    Top = 56
    Width = 67
    Height = 25
    Caption = #1042#1088#1077#1084#1103
    TabOrder = 3
  end
  object Panel3: TPanel
    Left = 242
    Top = 56
    Width = 66
    Height = 25
    Caption = #1054#1090#1074#1077#1090
    TabOrder = 4
  end
  object Panel4: TPanel
    Left = 308
    Top = 56
    Width = 67
    Height = 25
    Caption = #1055#1088'. '#1086#1090#1074#1077#1090
    TabOrder = 5
  end
  object Panel5: TPanel
    Left = 514
    Top = 56
    Width = 67
    Height = 25
    Caption = #1055#1088'. '#1086#1090#1074#1077#1090
    TabOrder = 6
  end
  object Panel6: TPanel
    Left = 448
    Top = 56
    Width = 66
    Height = 25
    Caption = #1054#1090#1074#1077#1090
    TabOrder = 7
  end
  object Panel7: TPanel
    Left = 381
    Top = 56
    Width = 67
    Height = 25
    Caption = #1042#1088#1077#1084#1103
    TabOrder = 8
  end
  object Panel8: TPanel
    Left = 721
    Top = 56
    Width = 67
    Height = 25
    Caption = #1055#1088'. '#1086#1090#1074#1077#1090
    TabOrder = 9
  end
  object Panel9: TPanel
    Left = 655
    Top = 56
    Width = 66
    Height = 25
    Caption = #1054#1090#1074#1077#1090
    TabOrder = 10
  end
  object Panel10: TPanel
    Left = 588
    Top = 56
    Width = 67
    Height = 25
    Caption = #1042#1088#1077#1084#1103
    TabOrder = 11
  end
  object Panel11: TPanel
    Left = 175
    Top = 24
    Width = 200
    Height = 31
    Caption = '10 '#1055#1086#1084#1077#1093
    TabOrder = 12
  end
  object Panel12: TPanel
    Left = 381
    Top = 24
    Width = 200
    Height = 31
    Caption = '15 '#1055#1086#1084#1077#1093
    TabOrder = 13
  end
  object Panel13: TPanel
    Left = 588
    Top = 24
    Width = 200
    Height = 31
    Caption = '25 '#1055#1086#1084#1077#1093
    TabOrder = 14
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 597
    Width = 812
    Height = 19
    Panels = <
      item
        Width = 50
      end>
    SimpleText = 'Some Of'
  end
  object StringGrid2: TStringGrid
    Left = 381
    Top = 87
    Width = 200
    Height = 392
    Cursor = crArrow
    Color = clBtnHighlight
    ColCount = 3
    DoubleBuffered = False
    FixedCols = 0
    RowCount = 15
    FixedRows = 0
    GridLineWidth = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine]
    ParentDoubleBuffered = False
    TabOrder = 16
    Touch.ParentTabletOptions = False
    Touch.TabletOptions = [toPressAndHold]
    OnDrawCell = StringGrid2DrawCell
    OnExit = StringGrid2Exit
    OnMouseWheelDown = StringGrid2MouseWheelDown
    OnMouseWheelUp = StringGrid2MouseWheelUp
    OnSelectCell = StringGrid2SelectCell
    ColWidths = (
      64
      64
      64)
  end
  object Panel14: TPanel
    Left = 24
    Top = 486
    Width = 145
    Height = 35
    Caption = #1042#1077#1088#1086#1103#1090#1085#1086#1089#1090#1100
    TabOrder = 17
  end
  object Panel15: TPanel
    Left = 24
    Top = 528
    Width = 145
    Height = 35
    Caption = #1057#1088#1077#1076#1085#1077#1077' '#1074#1088#1077#1084#1103
    TabOrder = 18
  end
  object StaticText1: TStaticText
    Left = 88
    Top = 93
    Width = 10
    Height = 17
    Caption = '1'
    TabOrder = 19
  end
  object StaticText2: TStaticText
    Left = 88
    Top = 119
    Width = 10
    Height = 17
    Caption = '2'
    TabOrder = 20
  end
  object StaticText3: TStaticText
    Left = 88
    Top = 145
    Width = 10
    Height = 17
    Caption = '3'
    TabOrder = 21
  end
  object StaticText4: TStaticText
    Left = 88
    Top = 171
    Width = 10
    Height = 17
    Caption = '4'
    TabOrder = 22
  end
  object StaticText5: TStaticText
    Left = 88
    Top = 197
    Width = 10
    Height = 17
    Caption = '5'
    TabOrder = 23
  end
  object StaticText6: TStaticText
    Left = 84
    Top = 431
    Width = 16
    Height = 17
    Caption = '14'
    TabOrder = 24
  end
  object StaticText7: TStaticText
    Left = 84
    Top = 405
    Width = 16
    Height = 17
    Caption = '13'
    TabOrder = 25
  end
  object StaticText8: TStaticText
    Left = 84
    Top = 379
    Width = 16
    Height = 17
    Caption = '12'
    TabOrder = 26
  end
  object StaticText9: TStaticText
    Left = 84
    Top = 353
    Width = 16
    Height = 17
    Caption = '11'
    TabOrder = 27
  end
  object StaticText10: TStaticText
    Left = 84
    Top = 327
    Width = 16
    Height = 17
    Caption = '10'
    TabOrder = 28
  end
  object StaticText11: TStaticText
    Left = 88
    Top = 301
    Width = 10
    Height = 17
    Caption = '9'
    TabOrder = 29
  end
  object StaticText12: TStaticText
    Left = 88
    Top = 275
    Width = 10
    Height = 17
    Caption = '8'
    TabOrder = 30
  end
  object StaticText13: TStaticText
    Left = 88
    Top = 249
    Width = 10
    Height = 17
    Caption = '7'
    TabOrder = 31
  end
  object StaticText14: TStaticText
    Left = 88
    Top = 223
    Width = 10
    Height = 17
    Caption = '6'
    TabOrder = 32
  end
  object StaticText15: TStaticText
    Left = 84
    Top = 458
    Width = 16
    Height = 17
    Caption = '15'
    TabOrder = 33
  end
  object probability10: TEdit
    Left = 176
    Top = 486
    Width = 200
    Height = 35
    Alignment = taCenter
    AutoSize = False
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    ReadOnly = True
    TabOrder = 34
  end
  object averageTime10: TEdit
    Left = 176
    Top = 528
    Width = 200
    Height = 35
    Alignment = taCenter
    AutoSize = False
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    ReadOnly = True
    TabOrder = 35
  end
  object probability15: TEdit
    Left = 381
    Top = 486
    Width = 200
    Height = 35
    Alignment = taCenter
    AutoSize = False
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    ReadOnly = True
    TabOrder = 36
  end
  object averageTime15: TEdit
    Left = 381
    Top = 528
    Width = 200
    Height = 35
    Alignment = taCenter
    AutoSize = False
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    ReadOnly = True
    TabOrder = 37
  end
  object probability25: TEdit
    Left = 588
    Top = 486
    Width = 200
    Height = 35
    Alignment = taCenter
    AutoSize = False
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    ReadOnly = True
    TabOrder = 38
  end
  object averageTime25: TEdit
    Left = 588
    Top = 528
    Width = 200
    Height = 35
    Alignment = taCenter
    AutoSize = False
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    ReadOnly = True
    TabOrder = 39
  end
  object RadioButton1: TRadioButton
    Left = 221
    Top = 569
    Width = 113
    Height = 17
    Caption = #1053#1077#1086#1075#1088#1072#1085#1080#1095#1077#1085#1085#1072#1103
    TabOrder = 40
    OnClick = RadioButton1Click
  end
  object RadioButton2: TRadioButton
    Left = 447
    Top = 571
    Width = 113
    Height = 17
    Caption = '7 '#1089#1077#1082#1091#1085#1076
    TabOrder = 41
    OnClick = RadioButton2Click
  end
  object RadioButton3: TRadioButton
    Left = 651
    Top = 571
    Width = 113
    Height = 17
    Caption = '3 '#1089#1077#1082#1091#1085#1076#1099
    TabOrder = 42
    OnClick = RadioButton3Click
  end
end
