//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "Unit1.h"
#include "Unit2.h"
#include "Unit3.h"
#include "Unit4.h"
#include "Unit5.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TBaseForm *BaseForm;
//---------------------------------------------------------------------------
UnicodeString ERROR_MESSAGE = "���� ��� ��������� ���������� ������������ �� ������!\r������� � ������ ��������?";
UnicodeString ERROR_TITLE = "������";
UnicodeString INFO_MESSAGE = "������ ���������� ������������?";
UnicodeString INFO_TITLE = "��������";
UnicodeString Status_1 = "��������� ������������ �� ������";
UnicodeString ERROR_CREATE_MESSAGE = "���� ������ �� ����� ���� �������.\n���������� ����� �������.";
//---------------------------------------------------------------------------
__fastcall TBaseForm::TBaseForm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TBaseForm::ChartsClick(TObject *Sender)
{
   //	Matrix->Cleaner();
	Matrix->ShowModal();
	Matrix->RadioButton1->Checked = false;
}
//---------------------------------------------------------------------------
void __fastcall TBaseForm::ExperimentClick(TObject *Sender)
{
	if ((Option->RadioGroup1->ItemIndex == -1) || (Option->RadioGroup2->ItemIndex == -1))
	{
	   if ((Application->MessageBox(ERROR_MESSAGE.w_str(), ERROR_TITLE.w_str(), MB_OKCANCEL|MB_ICONERROR))==IDOK)
	   {
		   Option->ShowModal();
       }
	}
	else
	{
		if ((Application->MessageBox(INFO_MESSAGE.w_str(), INFO_TITLE.w_str(),
		MB_OKCANCEL|MB_ICONINFORMATION))==IDOK)
		{
			Working->ShowModal();
		}
    }
}
//---------------------------------------------------------------------------
void __fastcall TBaseForm::OptionsClick(TObject *Sender)
{
	Option->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TBaseForm::GraphicsClick(TObject *Sender)
{
	ChartPlot->ShowModal();
}
//---------------------------------------------------------------------------
void __fastcall TBaseForm::TheoryClick(TObject *Sender)
{
   	AnsiString FilePath = ExtractFilePath(Application->ExeName);
   	ShowMessage(String(ExtractFilePath(Application->ExeName)));
	ShellExecute(Handle,L"open",L"Helper.chm",NULL,NULL,SW_RESTORE);
}
//---------------------------------------------------------------------------

void __fastcall TBaseForm::FormShow(TObject *Sender)
{
	BaseForm->StatusBar1->Panels->Items[0]->Text = Status_1;
}
//---------------------------------------------------------------------------

void __fastcall TBaseForm::FormCreate(TObject *Sender)
{
	char name[30] = "..\\info.db";
	char *zErrMsg = 0;
	int rc;
	rc = sqlite3_open(name, &db);
	if(rc) {
		Application->MessageBox(ERROR_CREATE_MESSAGE.w_str(), ERROR_TITLE.w_str(), MB_OK|MB_ICONERROR);
	  	Application->Terminate();
	}
	DbManager dataBase(db);
	if(!dataBase.checkExistTables()) {
		if(!dataBase.createTables()) {
			Application->MessageBox(ERROR_CREATE_MESSAGE.w_str(), ERROR_TITLE.w_str(), MB_OK|MB_ICONERROR);
			Application->Terminate();
		}
	}
}
//---------------------------------------------------------------------------

