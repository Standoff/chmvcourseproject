//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "Unit1.h"
#include "Unit2.h"
#include "Unit4.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TOption *Option;
//---------------------------------------------------------------------------
UnicodeString ERROR_MESSAGE = "���� ��� ��������� ���������� ������������ �� ������!";
UnicodeString ERROR_TITLE = "������";
UnicodeString Status_1 =  "��������� ������������ �� ������";
UnicodeString Status_2 =  "����������: ";
UnicodeString Status_3 =  "���������� �����: ";
UnicodeString Status_4 =  "�� ������";
UnicodeString Status_5 =  "�� ������";
//---------------------------------------------------------------------------
__fastcall TOption::TOption(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TOption::StatusDetecter()
{
	if ((RadioGroup1->ItemIndex == -1) && (RadioGroup2->ItemIndex == -1))
	{
		BaseForm->StatusBar1->Panels->Items[0]->Text = Status_1;
	}
	if ((RadioGroup1->ItemIndex != -1) && (RadioGroup2->ItemIndex == -1))
	{
		BaseForm->StatusBar1->Panels->Items[0]->Text = Status_2 +
		RadioGroup1->Items->Strings[RadioGroup1->ItemIndex] + " | " +
		Status_3 + Status_4;
	}
	if ((RadioGroup1->ItemIndex == -1) && (RadioGroup2->ItemIndex != -1))
	{
		BaseForm->StatusBar1->Panels->Items[0]->Text = Status_2 + Status_5 +
		" | " + Status_3 + RadioGroup2->Items->Strings[RadioGroup2->ItemIndex];
	}
	if ((RadioGroup1->ItemIndex != -1) && (RadioGroup2->ItemIndex != -1))
	{
		BaseForm->StatusBar1->Panels->Items[0]->Text = Status_2 +
		RadioGroup1->Items->Strings[RadioGroup1->ItemIndex] + " | " +
		Status_3 + RadioGroup2->Items->Strings[RadioGroup2->ItemIndex];
	}
}
//---------------------------------------------------------------------------
void __fastcall TOption::Button1Click(TObject *Sender)
{
	Option->Close();
	StatusDetecter();
}
//---------------------------------------------------------------------------
void __fastcall TOption::FormClose(TObject *Sender, TCloseAction &Action)
{
	StatusDetecter();
}
//---------------------------------------------------------------------------
void __fastcall TOption::Button2Click(TObject *Sender)
{
	if ((RadioGroup1->ItemIndex == -1) || (RadioGroup2->ItemIndex == -1))
	{
		Application->MessageBox(ERROR_MESSAGE.w_str(), ERROR_TITLE.w_str(), MB_OK|MB_ICONERROR);
	}
	if ((RadioGroup1->ItemIndex != -1) && (RadioGroup2->ItemIndex != -1))
	{
		Option->Close();
		Working->ShowModal();
	}
}
//---------------------------------------------------------------------------

