//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "Unit1.h"
#include "Unit2.h"
#include "Unit5.h"
#include "graphic.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "Chart"
#pragma link "Series"
#pragma link "TeEngine"
#pragma link "TeeProcs"
#pragma resource "*.dfm"
TChartPlot *ChartPlot;
//---------------------------------------------------------------------------
__fastcall TChartPlot::TChartPlot(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TChartPlot::btnClick(TObject *Sender)
{
	TChart * graphArea  = Chart1;
	//�������� �������
	TStringList * title = new TStringList();
	title->Add("����������� �������� ������� ����������� �������");
	title->Add("�� ���������� ����� �� ������ ���");
	title->Add("��������� ���������� �������");
	//�������� ��������
	vector<UnicodeString> seriesTitles;
	seriesTitles.push_back("�����.");
	seriesTitles.push_back("7 ���.");
	seriesTitles.push_back("3 ���.");

	//����� ��������
	ProcessInfo processInfo(Working->info);
	vector< vector<GraphPoint> > series = processInfo.calcPoints(ProcessInfo::CHART_AVERAGE_TIME_OF_QUANTITY_NOIZES);
	vector< vector<double> > calcMinMax = MathUtil::getMinMax(series);
	GraphPlot aa(graphArea);
	aa
		.setTitle(title)
		.setLeftAxisTitle("�����, �")
		.setBottomAxisTitle("���������� �����")
		.setSeriesTitles(seriesTitles)
		.setLeftAxisMinMax(calcMinMax[1][0], calcMinMax[1][1])
		.setBottomAxisMinMax(calcMinMax[0][0], calcMinMax[0][1])
		.setSeries(series)
		.plot();
}
//---------------------------------------------------------------------------
void __fastcall TChartPlot::FormCreate(TObject *Sender)
{
	TChart * graphArea  = Chart1;
	graphArea->Series[0]->Visible = false;
	graphArea->Series[1]->Visible = false;
	graphArea->Series[2]->Visible = false;
//	/***********************************************************************/
//	Working->info.getExposition(0).getExperiment(0).addCadr(2.41,0,0);
//	Working->info.getExposition(0).getExperiment(0).addCadr(2.75,0,0);
//	Working->info.getExposition(0).getExperiment(0).addCadr(4.62,0,0);
//	Working->info.getExposition(0).getExperiment(0).addCadr(2.42,0,0);
//	Working->info.getExposition(0).getExperiment(0).addCadr(8.84,0,0);
//	Working->info.getExposition(0).getExperiment(0).addCadr(2.58,0,0);
//	Working->info.getExposition(0).getExperiment(0).addCadr(4.01,3,3);
//	Working->info.getExposition(0).getExperiment(0).addCadr(2.09,0,0);
//	Working->info.getExposition(0).getExperiment(0).addCadr(2.63,3,3);
//	Working->info.getExposition(0).getExperiment(0).addCadr(1.31,2,2);
//	Working->info.getExposition(0).getExperiment(0).addCadr(2.64,0,0);
//	Working->info.getExposition(0).getExperiment(0).addCadr(2.2,4,4);
//	Working->info.getExposition(0).getExperiment(0).addCadr(3.13,1,1);
//	Working->info.getExposition(0).getExperiment(0).addCadr(5.99,3,3);
//	Working->info.getExposition(0).getExperiment(0).addCadr(1.64,1,1);
//	/***********************************************************************/
//	Working->info.getExposition(0).getExperiment(1).addCadr(3.13,0,0);
//	Working->info.getExposition(0).getExperiment(1).addCadr(3.24,0,0);
//	Working->info.getExposition(0).getExperiment(1).addCadr(5.71,4,4);
//	Working->info.getExposition(0).getExperiment(1).addCadr(7.42,0,2);
//	Working->info.getExposition(0).getExperiment(1).addCadr(4.06,2,2);
//	Working->info.getExposition(0).getExperiment(1).addCadr(3.13,0,0);
//	Working->info.getExposition(0).getExperiment(1).addCadr(4.01,2,0);
//	Working->info.getExposition(0).getExperiment(1).addCadr(4.34,4,4);
//	Working->info.getExposition(0).getExperiment(1).addCadr(5.38,0,0);
//	Working->info.getExposition(0).getExperiment(1).addCadr(2.8,0,0);
//	Working->info.getExposition(0).getExperiment(1).addCadr(4.12,0,0);
//	Working->info.getExposition(0).getExperiment(1).addCadr(2.91,2,2);
//	Working->info.getExposition(0).getExperiment(1).addCadr(2.53,3,3);
//	Working->info.getExposition(0).getExperiment(1).addCadr(13.68,0,3);
//	Working->info.getExposition(0).getExperiment(1).addCadr(1.48,2,2);
//	/***********************************************************************/
//	Working->info.getExposition(0).getExperiment(2).addCadr(3.41,0,0);
//	Working->info.getExposition(0).getExperiment(2).addCadr(5.66,0,0);
//	Working->info.getExposition(0).getExperiment(2).addCadr(8.79,4,4);
//	Working->info.getExposition(0).getExperiment(2).addCadr(7.8,3,3);
//	Working->info.getExposition(0).getExperiment(2).addCadr(7.03,3,3);
//	Working->info.getExposition(0).getExperiment(2).addCadr(8.4,4,4);
//	Working->info.getExposition(0).getExperiment(2).addCadr(7.69,1,1);
//	Working->info.getExposition(0).getExperiment(2).addCadr(3.9,1,1);
//	Working->info.getExposition(0).getExperiment(2).addCadr(2.09,4,4);
//	Working->info.getExposition(0).getExperiment(2).addCadr(10.21,0,3);
//	Working->info.getExposition(0).getExperiment(2).addCadr(6.59,2,0);
//	Working->info.getExposition(0).getExperiment(2).addCadr(10.55,2,2);
//	Working->info.getExposition(0).getExperiment(2).addCadr(7.14,3,0);
//	Working->info.getExposition(0).getExperiment(2).addCadr(8.02,0,0);
//	Working->info.getExposition(0).getExperiment(2).addCadr(0.94,0,0);
//	/***********************************************************************/
//	/***********************************************************************/
//
//	/***********************************************************************/
//	Working->info.getExposition(1).getExperiment(0).addCadr(0.55,0,0);
//	Working->info.getExposition(1).getExperiment(0).addCadr(2.58,0,0);
//	Working->info.getExposition(1).getExperiment(0).addCadr(3.40,3,3);
//	Working->info.getExposition(1).getExperiment(0).addCadr(5.16,2,2);
//	Working->info.getExposition(1).getExperiment(0).addCadr(3.57,4,4);
//	Working->info.getExposition(1).getExperiment(0).addCadr(4.56,0,0);
//	Working->info.getExposition(1).getExperiment(0).addCadr(3.52,4,4);
//	Working->info.getExposition(1).getExperiment(0).addCadr(3.85,0,0);
//	Working->info.getExposition(1).getExperiment(0).addCadr(2.48,1,1);
//	Working->info.getExposition(1).getExperiment(0).addCadr(1.81,3,3);
//	Working->info.getExposition(1).getExperiment(0).addCadr(2.96,0,0);
//	Working->info.getExposition(1).getExperiment(0).addCadr(2.31,4,4);
//	Working->info.getExposition(1).getExperiment(0).addCadr(2.09,0,0);
//	Working->info.getExposition(1).getExperiment(0).addCadr(3.4,0,0);
//	Working->info.getExposition(1).getExperiment(0).addCadr(2.64,4,4);
//	/***********************************************************************/
//	Working->info.getExposition(1).getExperiment(1).addCadr(4.89,0,0);
//	Working->info.getExposition(1).getExperiment(1).addCadr(5.22,3,3);
//	Working->info.getExposition(1).getExperiment(1).addCadr(4.29,0,0);
//	Working->info.getExposition(1).getExperiment(1).addCadr(3.35,0,0);
//	Working->info.getExposition(1).getExperiment(1).addCadr(7.08,4,4);
//	Working->info.getExposition(1).getExperiment(1).addCadr(4.12,0,0);
//	Working->info.getExposition(1).getExperiment(1).addCadr(3.13,3,3);
//	Working->info.getExposition(1).getExperiment(1).addCadr(3.84,1,1);
//	Working->info.getExposition(1).getExperiment(1).addCadr(2.91,2,2);
//	Working->info.getExposition(1).getExperiment(1).addCadr(1.65,1,1);
//	Working->info.getExposition(1).getExperiment(1).addCadr(2.85,0,0);
//	Working->info.getExposition(1).getExperiment(1).addCadr(2.64,3,3);
//	Working->info.getExposition(1).getExperiment(1).addCadr(5.22,0,0);
//	Working->info.getExposition(1).getExperiment(1).addCadr(2.15,1,1);
//	Working->info.getExposition(1).getExperiment(1).addCadr(3.74,0,0);
//	/***********************************************************************/
//	Working->info.getExposition(1).getExperiment(2).addCadr(0.38,2,0);
//	Working->info.getExposition(1).getExperiment(2).addCadr(4.01,0,0);
//	Working->info.getExposition(1).getExperiment(2).addCadr(3.73,0,2);
//	Working->info.getExposition(1).getExperiment(2).addCadr(3.35,1,0);
//	Working->info.getExposition(1).getExperiment(2).addCadr(5.11,1,0);
//	Working->info.getExposition(1).getExperiment(2).addCadr(4.89,2,3);
//	Working->info.getExposition(1).getExperiment(2).addCadr(15.65,0,0);
//	Working->info.getExposition(1).getExperiment(2).addCadr(3.52,1,0);
//	Working->info.getExposition(1).getExperiment(2).addCadr(2.36,1,0);
//	Working->info.getExposition(1).getExperiment(2).addCadr(4.95,3,0);
//	Working->info.getExposition(1).getExperiment(2).addCadr(5.11,0,0);
//	Working->info.getExposition(1).getExperiment(2).addCadr(2.74,3,3);
//	Working->info.getExposition(1).getExperiment(2).addCadr(1.76,2,0);
//	Working->info.getExposition(1).getExperiment(2).addCadr(4.12,3,0);
//	Working->info.getExposition(1).getExperiment(2).addCadr(3.46,4,2);
//	/***********************************************************************/
//	/***********************************************************************/
//
//	/***********************************************************************/
//	Working->info.getExposition(2).getExperiment(0).addCadr(0.44,0,0);
//	Working->info.getExposition(2).getExperiment(0).addCadr(2.09,0,0);
//	Working->info.getExposition(2).getExperiment(0).addCadr(2.09,0,0);
//	Working->info.getExposition(2).getExperiment(0).addCadr(2.36,0,0);
//	Working->info.getExposition(2).getExperiment(0).addCadr(2.14,0,0);
//	Working->info.getExposition(2).getExperiment(0).addCadr(4.12,0,0);
//	Working->info.getExposition(2).getExperiment(0).addCadr(2.64,1,1);
//	Working->info.getExposition(2).getExperiment(0).addCadr(1.64,2,2);
//	Working->info.getExposition(2).getExperiment(0).addCadr(2.58,4,4);
//	Working->info.getExposition(2).getExperiment(0).addCadr(2.26,0,0);
//	Working->info.getExposition(2).getExperiment(0).addCadr(1.87,0,0);
//	Working->info.getExposition(2).getExperiment(0).addCadr(2.42,0,0);
//	Working->info.getExposition(2).getExperiment(0).addCadr(2.31,0,0);
//	Working->info.getExposition(2).getExperiment(0).addCadr(2.48,4,4);
//	Working->info.getExposition(2).getExperiment(0).addCadr(1.92,0,0);
//	/***********************************************************************/
//	Working->info.getExposition(2).getExperiment(1).addCadr(2.85,0,0);
//	Working->info.getExposition(2).getExperiment(1).addCadr(2.64,4,4);
//	Working->info.getExposition(2).getExperiment(1).addCadr(2.7,0,4);
//	Working->info.getExposition(2).getExperiment(1).addCadr(2.31,2,2);
//	Working->info.getExposition(2).getExperiment(1).addCadr(1.92,0,0);
//	Working->info.getExposition(2).getExperiment(1).addCadr(3.08,4,4);
//	Working->info.getExposition(2).getExperiment(1).addCadr(2.58,0,2);
//	Working->info.getExposition(2).getExperiment(1).addCadr(2.91,0,0);
//	Working->info.getExposition(2).getExperiment(1).addCadr(2.47,2,2);
//	Working->info.getExposition(2).getExperiment(1).addCadr(2.31,3,3);
//	Working->info.getExposition(2).getExperiment(1).addCadr(3.29,4,4);
//	Working->info.getExposition(2).getExperiment(1).addCadr(5.71,0,4);
//	Working->info.getExposition(2).getExperiment(1).addCadr(1.65,1,0);
//	Working->info.getExposition(2).getExperiment(1).addCadr(2.52,4,4);
//	Working->info.getExposition(2).getExperiment(1).addCadr(2.63,0,0);
//	/***********************************************************************/
//	Working->info.getExposition(2).getExperiment(2).addCadr(0.39,1,3);
//	Working->info.getExposition(2).getExperiment(2).addCadr(2.86,4,0);
//	Working->info.getExposition(2).getExperiment(2).addCadr(2.86,2,0);
//	Working->info.getExposition(2).getExperiment(2).addCadr(2.75,0,0);
//	Working->info.getExposition(2).getExperiment(2).addCadr(2.14,1,1);
//	Working->info.getExposition(2).getExperiment(2).addCadr(3.08,3,1);
//	Working->info.getExposition(2).getExperiment(2).addCadr(1.48,2,2);
//	Working->info.getExposition(2).getExperiment(2).addCadr(2.52,0,0);
//	Working->info.getExposition(2).getExperiment(2).addCadr(3.21,0,1);
//	Working->info.getExposition(2).getExperiment(2).addCadr(2.91,1,4);
//	Working->info.getExposition(2).getExperiment(2).addCadr(1.54,3,0);
//	Working->info.getExposition(2).getExperiment(2).addCadr(6.7,0,0);
//	Working->info.getExposition(2).getExperiment(2).addCadr(10.71,0,2);
//	Working->info.getExposition(2).getExperiment(2).addCadr(1.81,0,0);
//	Working->info.getExposition(2).getExperiment(2).addCadr(2.36,3,2);
//	/***********************************************************************/
//	//������ ������ �����������
//	Working->info.getExposition(2).deleteExperiment(2);
//
//	/***********************************************************************/
//	//������� ������ ���������� ���� ���
//	/***********************************************************************/
//	Working->info.getExposition(2).getExperiment(2).addCadr(0.39,1,3);
//	Working->info.getExposition(2).getExperiment(2).addCadr(2.86,4,0);
//	Working->info.getExposition(2).getExperiment(2).addCadr(2.86,2,0);
//	Working->info.getExposition(2).getExperiment(2).addCadr(2.75,0,0);
//	Working->info.getExposition(2).getExperiment(2).addCadr(2.14,1,1);
//	Working->info.getExposition(2).getExperiment(2).addCadr(3.08,3,1);
//	Working->info.getExposition(2).getExperiment(2).addCadr(1.48,2,2);
//	Working->info.getExposition(2).getExperiment(2).addCadr(2.52,0,0);
//	Working->info.getExposition(2).getExperiment(2).addCadr(3.21,0,1);
//	Working->info.getExposition(2).getExperiment(2).addCadr(2.91,1,4);
//	Working->info.getExposition(2).getExperiment(2).addCadr(1.54,3,0);
//	Working->info.getExposition(2).getExperiment(2).addCadr(6.7,0,0);
//	Working->info.getExposition(2).getExperiment(2).addCadr(10.71,0,2);
//	Working->info.getExposition(2).getExperiment(2).addCadr(1.81,0,0);
//	Working->info.getExposition(2).getExperiment(2).addCadr(2.36,3,2);
}
//---------------------------------------------------------------------------
void __fastcall TChartPlot::BitBtn1Click(TObject *Sender)
{
	 TChart * graphArea  = Chart1;
//	graphArea->Series[0] = o;
	GraphPlot aa(graphArea);
	//�������� �������
	TStringList * title = new TStringList();
	title->Add("����������� ������� ����������� �������");
	title->Add("�� ���������� ����� �� ������ ���");
	title->Add("��������� ���������� �������");
	//�������� ��������
	vector<UnicodeString> seriesTitles;
	seriesTitles.push_back("�����.");
	seriesTitles.push_back("7 ���.");
	seriesTitles.push_back("3 ���.");
	//����� ��������
	ProcessInfo processInfo(Working->info);
	vector< vector<GraphPoint> > series = processInfo.calcPoints(ProcessInfo::CHART_FREQUENCY_DETECTION_OF_QUANTITY_NOIZES);
	vector< vector<double> > calcMinMax = MathUtil::getMinMax(series);
	aa
		.setTitle(title)
		.setLeftAxisTitle("�����, �")
		.setBottomAxisTitle("���������� �����")
		.setSeriesTitles(seriesTitles)
		.setLeftAxisMinMax(calcMinMax[1][0], calcMinMax[1][1])
		.setBottomAxisMinMax(calcMinMax[0][0], calcMinMax[0][1])
		.setSeries(series)
		.plot();
}
//---------------------------------------------------------------------------
void __fastcall TChartPlot::BitBtn2Click(TObject *Sender)
{
	 TChart * graphArea  = Chart1;
	TStringList * title = new TStringList();
	title->Add("����������� �������� ������� ����������� �������");
	title->Add("�� ������������ ����������");
	//�������� ��������
	vector<UnicodeString> seriesTitles;
	seriesTitles.push_back("10 �����");
	seriesTitles.push_back("15 �����");
	seriesTitles.push_back("25 �����");
	//����� ��������
	ProcessInfo processInfo(Working->info);
	vector< vector<GraphPoint> > series = processInfo.calcPoints(ProcessInfo::CHART_AVERAGE_TIME_OF_DURATION_EXPOSITIONS);
	vector< vector<double> > calcMinMax = MathUtil::getMinMax(series);
	GraphPlot aa(graphArea);
	aa
		.setTitle(title)
		.setLeftAxisTitle("�����, �")
		.setBottomAxisTitle("������������ ����������")
		.setSeriesTitles(seriesTitles)
		.setLeftAxisMinMax(calcMinMax[1][0], calcMinMax[1][1])
		.setBottomAxisMinMax(calcMinMax[0][0], calcMinMax[0][1])
		.setSeries(series)
		.plot();
}
//---------------------------------------------------------------------------
void __fastcall TChartPlot::Button1Click(TObject *Sender)
{
	TChart * graphArea  = Chart1;
	TStringList * title = new TStringList();
	title->Add("����������� ������� ����������� �������");
	title->Add("�� ������������ ����������");
	//�������� ��������
	vector<UnicodeString> seriesTitles;
	seriesTitles.push_back("10 �����");
	seriesTitles.push_back("15 �����");
	seriesTitles.push_back("25 �����");
	//����� ��������
	ProcessInfo processInfo(Working->info);
	vector< vector<GraphPoint> > series = processInfo.calcPoints(ProcessInfo::CHART_FREQUENCY_DETECTION_OF_DURATION_EXPOSITIONS);
	vector< vector<double> > calcMinMax = MathUtil::getMinMax(series);
	GraphPlot aa(graphArea);
	aa
		.setTitle(title)
		.setLeftAxisTitle("������� �����������")
		.setBottomAxisTitle("������������ ����������")
		.setSeriesTitles(seriesTitles)
		.setLeftAxisMinMax(calcMinMax[1][0], calcMinMax[1][1])
		.setBottomAxisMinMax(calcMinMax[0][0], calcMinMax[0][1])
		.setSeries(series)
		.plot();
}
//---------------------------------------------------------------------------
void __fastcall TChartPlot::Button2Click(TObject *Sender)
{
	TChart * graphArea  = Chart1;
	TStringList * title = new TStringList();
	title->Add("����������� ������");
	title->Add("�� �������������� ������� �� ������");
	//�������� ��������
	vector<UnicodeString> seriesTitles;
	seriesTitles.push_back("�������� �������");
	seriesTitles.push_back("������ �������");
	//����� ��������
	ProcessInfo processInfo(Working->info);
	vector< vector<GraphPoint> > series = processInfo.calcPoints(ProcessInfo::CHART_QUANTITY_ERRORS_OF_NUMBER_DISPLAY);
	vector< vector<double> > calcMinMax = MathUtil::getMinMax(series);
	GraphPlot aa(graphArea);
	aa
		.setTitle(title)
		.setLeftAxisTitle("���������� ������ �� ���� ������")
		.setBottomAxisTitle("������ ����������")
		.setSeriesTitles(seriesTitles)
		.setLeftAxisMinMax(calcMinMax[1][0], calcMinMax[1][1])
		.setBottomAxisMinMax(calcMinMax[0][0], calcMinMax[0][1])
		.setSeries(series)
		.plot();
}
//---------------------------------------------------------------------------



