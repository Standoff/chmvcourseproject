//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "Unit1.h"
#include "Unit2.h"
#include "Unit3.h"
#include "Unit4.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMatrix *Matrix;
TGridRect GridRect;
//---------------------------------------------------------------------------
int n = 0,v = 0;
bool Enable_Label = false;
//---------------------------------------------------------------------------
__fastcall TMatrix::TMatrix(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TMatrix::OptionChecker()
{
	if (Option->CheckBox1->Checked==true)
	{
		Enable_Label = true;
	}
}
//---------------------------------------------------------------------------
void TMatrix::Cleaner()                             // ������� �����
{
	int i, v;
	for (i = 0; i < 3; i++)
	{
		for (v = 0; v < 15; v++)
		{
			StringGrid1->Cells[i][v] = "";
			StringGrid2->Cells[i][v] = "";
			StringGrid3->Cells[i][v] = "";
		}
	}
	probability10->Text = "";
	probability15->Text = "";
	probability25->Text = "";
	averageTime10->Text = "";
	averageTime15->Text = "";
	averageTime25->Text = "";
}
//---------------------------------------------------------------------------
void __fastcall TMatrix::StringGrid1MouseWheelUp(TObject *Sender, TShiftState Shift,
		  TPoint &MousePos, bool &Handled)
{
	Handled = True;
}
//---------------------------------------------------------------------------
void __fastcall TMatrix::StringGrid1MouseWheelDown(TObject *Sender, TShiftState Shift,
          TPoint &MousePos, bool &Handled)
{
	Handled = True;
}
//---------------------------------------------------------------------------
void __fastcall TMatrix::FormShow(TObject *Sender)
{
	GridRect.Top = -1;
	GridRect.Left = -1;
	GridRect.Right = -1;
	GridRect.Bottom = -1;
	StringGrid1->Selection = GridRect;
	StringGrid2->Selection = GridRect;
	StringGrid3->Selection = GridRect;
}
//---------------------------------------------------------------------------
void __fastcall TMatrix::StringGrid2MouseWheelDown(TObject *Sender, TShiftState Shift,
          TPoint &MousePos, bool &Handled)
{
	Handled = True;
}
//---------------------------------------------------------------------------

void __fastcall TMatrix::StringGrid2MouseWheelUp(TObject *Sender, TShiftState Shift,
		  TPoint &MousePos, bool &Handled)
{
	Handled = True;
}
//---------------------------------------------------------------------------
void __fastcall TMatrix::StringGrid3MouseWheelDown(TObject *Sender, TShiftState Shift,
		  TPoint &MousePos, bool &Handled)
{
	Handled = True;
}
//---------------------------------------------------------------------------
void __fastcall TMatrix::StringGrid3MouseWheelUp(TObject *Sender, TShiftState Shift,
		  TPoint &MousePos, bool &Handled)
{
	Handled = True;
}
//---------------------------------------------------------------------------
void __fastcall TMatrix::StringGrid4MouseWheelDown(TObject *Sender, TShiftState Shift,
		  TPoint &MousePos, bool &Handled)
{
	Handled = True;
}
//---------------------------------------------------------------------------
void __fastcall TMatrix::StringGrid4MouseWheelUp(TObject *Sender, TShiftState Shift,
		  TPoint &MousePos, bool &Handled)
{
	Handled = True;
}
//---------------------------------------------------------------------------
void __fastcall TMatrix::StringGrid1Exit(TObject *Sender)
{
	GridRect.Top = -1;
	GridRect.Left = -1;
	GridRect.Right = -1;
	GridRect.Bottom = -1;
	StringGrid1->Selection = GridRect;
}
//---------------------------------------------------------------------------
void __fastcall TMatrix::StringGrid2Exit(TObject *Sender)
{
	GridRect.Top = -1;
	GridRect.Left = -1;
	GridRect.Right = -1;
	GridRect.Bottom = -1;
	StringGrid2->Selection = GridRect;
}
//---------------------------------------------------------------------------
void __fastcall TMatrix::StringGrid3Exit(TObject *Sender)
{
	GridRect.Top = -1;
	GridRect.Left = -1;
	GridRect.Right = -1;
	GridRect.Bottom = -1;
	StringGrid3->Selection = GridRect;
}
//---------------------------------------------------------------------------
void __fastcall TMatrix::StringGrid1DrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect,
          TGridDrawState State)
{
	if (Enable_Label == true)
	{
		if (StringGrid1->Cells[1][ARow]==StringGrid1->Cells[2][ARow])
		{
			if (ACol == 1)
			{
				StringGrid1->Canvas->Brush->Color = clGreen;
				StringGrid1->Canvas->FillRect(Rect);
			}
		}
		else
		{
			if (ACol == 1)
			{
				StringGrid1->Canvas->Brush->Color = clRed;
				StringGrid1->Canvas->FillRect(Rect);
			}
		}
	}
//	UINT TextFlags = DT_CENTER | DT_VCENTER | DT_SINGLELINE;
//	((TStringGrid*)Sender)->Canvas->FillRect(Rect);
//	DrawText(((TStringGrid*)Sender)->Canvas->Handle,
//	((TStringGrid*)Sender)->Cells[ACol][ARow].c_str(),
//	((TStringGrid*)Sender)->Cells[ACol][ARow].Length(), &Rect,TextFlags);
}
//---------------------------------------------------------------------------
void __fastcall TMatrix::StringGrid2DrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect,
          TGridDrawState State)
{
	if (Enable_Label == true)
	{
		if (StringGrid2->Cells[1][ARow]==StringGrid2->Cells[2][ARow])
		{
			if (ACol == 1)
			{
				StringGrid2->Canvas->Brush->Color = clGreen;
				StringGrid2->Canvas->FillRect(Rect);
			}
		}
		else
		{
			if (ACol == 1)
			{
				StringGrid2->Canvas->Brush->Color = clRed;
				StringGrid2->Canvas->FillRect(Rect);
			}
		}
	}
	UINT TextFlags = DT_CENTER | DT_VCENTER | DT_SINGLELINE;
	((TStringGrid*)Sender)->Canvas->FillRect(Rect);
	DrawText(((TStringGrid*)Sender)->Canvas->Handle,
	((TStringGrid*)Sender)->Cells[ACol][ARow].c_str(),
	((TStringGrid*)Sender)->Cells[ACol][ARow].Length(), &Rect,TextFlags);
}
//---------------------------------------------------------------------------
void __fastcall TMatrix::StringGrid3DrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect,
          TGridDrawState State)
{
	if (Enable_Label == true)
	{
		if (StringGrid3->Cells[1][ARow]==StringGrid3->Cells[2][ARow])
		{
			if (ACol == 1)
			{
				StringGrid3->Canvas->Brush->Color = clGreen;
				StringGrid3->Canvas->FillRect(Rect);
			}
		}
		else
		{
			if (ACol == 1)
			{
				StringGrid3->Canvas->Brush->Color = clRed;
				StringGrid3->Canvas->FillRect(Rect);
			}
		}
	}
	UINT TextFlags = DT_CENTER | DT_VCENTER | DT_SINGLELINE;
	((TStringGrid*)Sender)->Canvas->FillRect(Rect);
	DrawText(((TStringGrid*)Sender)->Canvas->Handle,
	((TStringGrid*)Sender)->Cells[ACol][ARow].c_str(),
	((TStringGrid*)Sender)->Cells[ACol][ARow].Length(), &Rect,TextFlags);
}
//---------------------------------------------------------------------------
void __fastcall TMatrix::StringGrid1SelectCell(TObject *Sender, int ACol, int ARow,
          bool &CanSelect)
{
	CanSelect = false;
}
//---------------------------------------------------------------------------

void __fastcall TMatrix::StringGrid2SelectCell(TObject *Sender, int ACol, int ARow,
          bool &CanSelect)
{
	CanSelect = false;
}
//---------------------------------------------------------------------------
void __fastcall TMatrix::StringGrid3SelectCell(TObject *Sender, int ACol, int ARow,
		  bool &CanSelect)
{
	CanSelect = false;
}
//---------------------------------------------------------------------------
void TMatrix::Drawer(TStringGrid *StringGrid, int Cadr, int Experiment, int Exposition)
{
	if (!(Working->info.getExposition(Exposition).containExperiment(Experiment)))
	{
		StringGrid->Cells[0][Cadr] = FloatToStr (
			Working->info
				.getExposition(Exposition)
				.getExperiment(Experiment)
				.getCadr(Cadr)
				.getTime()
		);
		StringGrid->Cells[1][Cadr] = FloatToStr (
			Working->info
				.getExposition(Exposition)
				.getExperiment(Experiment)
				.getCadr(Cadr)
				.getManAnswer()
		);
		StringGrid->Cells[2][Cadr] = FloatToStr (
			Working->info
				.getExposition(Exposition)
				.getExperiment(Experiment)
				.getCadr(Cadr)
				.getCorrectAnswer()
		);
    }
}
//---------------------------------------------------------------------------
void __fastcall TMatrix::RadioButton1Click(TObject *Sender)
{
	OptionChecker();
	Cleaner();
	for(int i = 0; i < Experiment::COUNT_CARDS; i++ )
	{
		{
			Drawer(StringGrid1, i, Exposition::INFINITY_EXPOSITION, Experiment::TEN_NOIZES);
			Drawer(StringGrid2, i, Exposition::SEVEN_EXPOSITION, Experiment::TEN_NOIZES);
			Drawer(StringGrid3, i, Exposition::THREE_EXPOSITION, Experiment::TEN_NOIZES);
		}
	}
	DrawerBottomValues(probability10, averageTime10, Exposition::INFINITY_EXPOSITION, Experiment::TEN_NOIZES);
	DrawerBottomValues(probability15, averageTime15, Exposition::SEVEN_EXPOSITION, Experiment::TEN_NOIZES);
	DrawerBottomValues(probability25, averageTime25, Exposition::THREE_EXPOSITION, Experiment::TEN_NOIZES);
}
//---------------------------------------------------------------------------
void __fastcall TMatrix::RadioButton2Click(TObject *Sender)
{
	OptionChecker();
	Cleaner();
	for(int i = 0; i < Experiment::COUNT_CARDS; i++ )
	{
		{
			Drawer(StringGrid1, i, Exposition::INFINITY_EXPOSITION, Experiment::FIFTEEN_NOIZES);
			Drawer(StringGrid2, i, Exposition::SEVEN_EXPOSITION, Experiment::FIFTEEN_NOIZES);
			Drawer(StringGrid3, i, Exposition::THREE_EXPOSITION, Experiment::FIFTEEN_NOIZES);
		}
	}
	DrawerBottomValues(probability10, averageTime10, Exposition::INFINITY_EXPOSITION, Experiment::FIFTEEN_NOIZES);
	DrawerBottomValues(probability15, averageTime15, Exposition::SEVEN_EXPOSITION, Experiment::FIFTEEN_NOIZES);
	DrawerBottomValues(probability25, averageTime25, Exposition::THREE_EXPOSITION, Experiment::FIFTEEN_NOIZES);
}
//---------------------------------------------------------------------------
void __fastcall TMatrix::RadioButton3Click(TObject *Sender)
{
	OptionChecker();
	Cleaner();
	for(int i = 0; i < Experiment::COUNT_CARDS; i++ )
	{
		{
			Drawer(StringGrid1, i, Exposition::INFINITY_EXPOSITION, Experiment::TWENTYFIVE_NOIZES);
			Drawer(StringGrid2, i, Exposition::SEVEN_EXPOSITION, Experiment::TWENTYFIVE_NOIZES);
			Drawer(StringGrid3, i, Exposition::THREE_EXPOSITION, Experiment::TWENTYFIVE_NOIZES);
		}
	}
	DrawerBottomValues(probability10, averageTime10, Exposition::INFINITY_EXPOSITION, Experiment::TWENTYFIVE_NOIZES);
	DrawerBottomValues(probability15, averageTime15, Exposition::SEVEN_EXPOSITION, Experiment::TWENTYFIVE_NOIZES);
	DrawerBottomValues(probability25, averageTime25, Exposition::THREE_EXPOSITION, Experiment::TWENTYFIVE_NOIZES);
}
//---------------------------------------------------------------------------
void TMatrix::DrawerBottomValues(TEdit *Probability, TEdit *AverageTime, int exprt, int expstn)
{
  	ProcessInfo processInfo(Working->info);
	  if(!Working->info
			.getExposition(expstn)
			.getExperiment(exprt).isEmpty()
		)
	{
		Probability->Text = FormatFloat("0.0000",processInfo.calcProbability(expstn, exprt));
		AverageTime->Text = FormatFloat("0.0000",processInfo.calcAverageTime(expstn, exprt));
	}
}
//---------------------------------------------------------------------------
void __fastcall TMatrix::FormClose(TObject *Sender, TCloseAction &Action)
{
	Cleaner();
 	Enable_Label = false;
	Matrix->Repaint();
}
//---------------------------------------------------------------------------

