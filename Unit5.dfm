object ChartPlot: TChartPlot
  Left = 0
  Top = 0
  Caption = #1054#1090#1086#1073#1088#1072#1078#1077#1085#1080#1077' '#1075#1088#1072#1092#1080#1082#1086#1074
  ClientHeight = 536
  ClientWidth = 766
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object btn: TBitBtn
    Left = 8
    Top = 504
    Width = 145
    Height = 25
    Caption = #1058#1080#1087' '#1075#1088#1072#1092#1080#1082#1072' 1'
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 0
    OnClick = btnClick
  end
  object Chart1: TChart
    Left = 8
    Top = 8
    Width = 745
    Height = 491
    Cursor = crArrow
    AllowPanning = pmNone
    Legend.DividingLines.Width = 3
    Legend.Font.Height = -13
    Legend.LegendStyle = lsSeries
    Legend.PositionUnits = muPercent
    Title.Font.Color = -1
    Title.Text.Strings = (
      #1053#1072#1079#1074#1072#1085#1080#1077' '#1075#1088#1072#1092#1080#1082#1072)
    BottomAxis.Automatic = False
    BottomAxis.AutomaticMaximum = False
    BottomAxis.AutomaticMinimum = False
    BottomAxis.ExactDateTime = False
    BottomAxis.Grid.Color = clSilver
    BottomAxis.Grid.Style = psSolid
    BottomAxis.Increment = 1.000000000000000000
    BottomAxis.Maximum = 50.000000000000000000
    BottomAxis.MinimumRound = True
    LeftAxis.Automatic = False
    LeftAxis.AutomaticMaximum = False
    LeftAxis.AutomaticMinimum = False
    LeftAxis.Grid.Style = psSolid
    LeftAxis.Grid.ZPosition = 1.000000000000000000
    LeftAxis.Maximum = 30.000000000000000000
    LeftAxis.Minimum = -10.000000000000000000
    LeftAxis.MinimumRound = True
    RightAxis.Automatic = False
    RightAxis.AutomaticMaximum = False
    RightAxis.AutomaticMinimum = False
    TopAxis.Automatic = False
    TopAxis.AutomaticMaximum = False
    TopAxis.AutomaticMinimum = False
    View3D = False
    Zoom.Allow = False
    TabOrder = 1
    ColorPaletteIndex = 13
    object Series1: TLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Shadow.Color = 8553090
      Marks.Visible = False
      Title = #1043#1088#1072#1092#1080#1082' 1'
      LinePen.Color = clRed
      LinePen.Width = 3
      LinePen.EndStyle = esSquare
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = True
      TreatNulls = tnIgnore
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object Series2: TLineSeries
      Cursor = crArrow
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      Title = #1043#1088#1072#1092#1080#1082' 2'
      LinePen.Width = 3
      Pointer.InflateMargins = True
      Pointer.Style = psDownTriangle
      Pointer.Visible = True
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object Series3: TLineSeries
      Marks.Arrow.Visible = True
      Marks.Callout.Brush.Color = clBlack
      Marks.Callout.Arrow.Visible = True
      Marks.Visible = False
      SeriesColor = 10485760
      Title = #1043#1088#1072#1092#1080#1082' 3'
      LinePen.Width = 3
      Pointer.InflateMargins = True
      Pointer.Style = psTriangle
      Pointer.Visible = True
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
  end
  object BitBtn1: TBitBtn
    Left = 176
    Top = 505
    Width = 145
    Height = 25
    Caption = #1058#1080#1087' '#1075#1088#1072#1092#1080#1082#1072' 2'
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 2
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 346
    Top = 505
    Width = 136
    Height = 25
    Caption = #1058#1080#1087' '#1075#1088#1072#1092#1080#1082#1072' 3'
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 3
    OnClick = BitBtn2Click
  end
  object Button1: TButton
    Left = 488
    Top = 505
    Width = 126
    Height = 25
    Caption = #1058#1080#1087' '#1075#1088#1072#1092#1080#1082#1072' 4'
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 628
    Top = 505
    Width = 125
    Height = 25
    Caption = #1058#1080#1087' '#1075#1088#1072#1092#1080#1082#1072' 5'
    TabOrder = 5
    OnClick = Button2Click
  end
end
