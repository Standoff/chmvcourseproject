object BaseForm: TBaseForm
  Left = 343
  Top = 257
  HelpType = htKeyword
  Caption = 'BaseForm'
  ClientHeight = 484
  ClientWidth = 823
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 465
    Width = 823
    Height = 19
    Panels = <
      item
        Alignment = taCenter
        Width = 50
      end>
  end
  object MainMenu1: TMainMenu
    Left = 48
    Top = 32
    object Options: TMenuItem
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
      OnClick = OptionsClick
    end
    object Work: TMenuItem
      Caption = #1056#1072#1073#1086#1090#1072
      object Experiment: TMenuItem
        Caption = #1069#1082#1089#1087#1077#1088#1080#1084#1077#1085#1090
        OnClick = ExperimentClick
      end
      object Theory: TMenuItem
        Caption = #1058#1077#1086#1088#1080#1103
        OnClick = TheoryClick
      end
    end
    object Results: TMenuItem
      Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090#1099
      object Charts: TMenuItem
        Caption = #1058#1072#1073#1083#1080#1094#1099
        OnClick = ChartsClick
      end
      object Graphics: TMenuItem
        Caption = #1043#1088#1072#1092#1080#1082#1080
        OnClick = GraphicsClick
      end
    end
  end
end
